import React from 'react'
import { shallow, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import AssignmentDashboard from './assignment-dashboard'
import MessageService from '../service/message-service'

const messageService = new MessageService()

configure({ adapter: new Adapter() })

describe('<AssignmentDashboard />', () => {
  it('should render a dashboard page without crashing', () => {
    shallow(<AssignmentDashboard messageService={messageService} />)
  })

  it('should display an empty table', () => {
    const wrap = shallow(<AssignmentDashboard messageService={messageService} />)
    expect(wrap.find('Table.Body')).toHaveLength(0)
  })
})
