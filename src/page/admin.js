import React, { Component } from 'react'
import { Button, Card, Confirm, Form, Header } from 'semantic-ui-react'
import './margins.css'

const reject = require('lodash.reject')

const formOptions = [
  {
    key: 'Input',
    text: 'Input',
    value: 'Input'
  },
  {
    key: 'Checkbox',
    text: 'Checkbox',
    value: 'Checkbox'
  }
]

const inspectionOptions = [
  {
    key: 'Operator',
    text: 'Operator',
    value: 'Operator'
  },
  {
    key: 'Inspector',
    text: 'Inspector',
    value: 'Inspector'
  }
]

class Admin extends Component {
  constructor (props) {
    super(props)
    this.state = {
      service: this.props.service,
      messageService: this.props.messageService,
      data: [],
      jobNumber: this.props.jobNumber,
      name: '',
      inspectionNumber: '',
      frequency: '',
      setupTech: '',
      method: '',
      expected: '',
      tolerance: '',
      formType: '',
      inpectionType: ''
    }

    this.handleChange = this.handleChange.bind(this)
    this.handleAdd = this.handleAdd.bind(this)
    this.handleRemove = this.handleRemove.bind(this)
    this.handleClose = this.handleClose.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleOpen = this.handleOpen.bind(this)
    this.handleSelect = this.handleSelect.bind(this)
    this.handleInspectionTypeSelect = this.handleInspectionTypeSelect.bind(this)
  }

  componentDidMount () {
    if (typeof (this.state.service) !== 'undefined' && this.state.data.length === 0) {
      this.state.service.getInstructions(this.state.jobNumber, this.state.data)
        .then((data) => {
          if (typeof (data) !== 'undefined' && typeof (data.characteristics) !== 'undefined') {
            this.setState({
              data: data.characteristics
            })
          }
        })
        .catch((err) => {
          this.setState({ open: false })
          this.state.messageService.showMessage(`Error: ${err}`)
        })
    }
  }

  handleSubmit (e) {
    const data = {
      characteristics: this.state.data
    }
    this.state.service.putInstructions(this.state.jobNumber, data)
      .then(() => {
        this.state.messageService.showMessage('Success')
        this.handleClose()
      })
      .catch((err) => {
        this.setState({ open: false })
        this.state.messageService.showMessage(`Error: ${err}`)
      })
  }

  handleChange (e, { name, value }) {
    this.setState({ [name]: value })
  }

  handleRemove (nameOfCharacteristic) {
    const dataAfterRemoved = reject(this.state.data, { name: nameOfCharacteristic })
    this.setState({
      data: dataAfterRemoved
    })
  }

  handleOpen () {
    this.setState({ open: true })
  }

  handleClose () {
    this.setState({ open: false })
  }

  handleAdd () {
    const characteristic = {
      job_number: this.state.jobNumber,
      name: this.state.name,
      inspection_number: this.state.inspectionNumber,
      frequency: this.state.frequency,
      setup_tech: this.state.setupTech,
      method: this.state.method,
      expected: this.state.expected,
      tolerance: this.state.tolerance,
      form_type: this.state.formType,
      inspection_type: this.state.inspectionType
    }

    this.setState(prevState => ({
      data: [...prevState.data, characteristic],
      name: '',
      inspectionNumber: '',
      frequency: '',
      setupTech: '',
      method: '',
      expected: '',
      tolerance: '',
      formType: '',
      inpectionType: ''
    }))
  }

  handleSelect (e, { value }) {
    this.setState({
      formType: value
    })
  }

  handleInspectionTypeSelect (e, { value }) {
    this.setState({
      inspectionType: value
    })
  }

  render () {
    return (
      <div className='PageLayout' id='admin'>
        <Form onSubmit={this.handleAdd}>
          <Header as='h1'>Add Characteristics to Job Number: {this.state.jobNumber}</Header>
          <Form.Input required id='characteristicNameInput' onChange={this.handleChange} value={this.state.name} name='name' placeholder='Characteristic Name' label='Charcteristic Name' />
          <Form.Input required id='inspectionNumberInput' onChange={this.handleChange} value={this.state.inspectionNumber} name='inspectionNumber' placeholder='Inspection Number' label='Inspection Number' />
          <Form.Input required id='frequencyInput' onChange={this.handleChange} value={this.state.frequency} name='frequency' placeholder='Frequency' label='Frequency' />
          <Form.Input required id='setupTechInput' onChange={this.handleChange} value={this.state.setupTech} name='setupTech' placeholder='Setup Technician' label='Setup Technician' />
          <Form.Input required id='methodInput' onChange={this.handleChange} value={this.state.method} name='method' placeholder='Method' label='Method' />
          <Form.Input required id='expectedValueInput' onChange={this.handleChange} value={this.state.expected} name='expected' placeholder='Expected Value' label='Expected Value' />
          <Form.Input required id='toleranceValueInput' onChange={this.handleChange} value={this.state.tolerance} name='tolerance' placeholder='Tolerance' label='Tolerance' />
          <Form.Select
            required
            id='formTypeInput'
            onChange={this.handleSelect}
            placeholder='Form Type'
            label='Form Type'
            options={formOptions}
          />
          <Form.Select
            required
            id='formTypeInput'
            onChange={this.handleInspectionTypeSelect}
            placeholder='Inspection Type'
            label='Inspection Type'
            options={inspectionOptions}
          />
          <Form.Button primary id='buttonAdd'>Add </Form.Button>
          <Header as='h2'>Characteristics</Header>
          {this.state.data.length > 0 &&
            <Card.Group>
              {this.state.data.map((characteristic, i) => {
                return (
                  <Card key={i} className='Card'>
                    <Card.Content>
                      <Card.Header>
                        {characteristic.name}
                      </Card.Header>
                      <Card.Description>
                        Inspection Number: {characteristic.inspection_number} <br />
                        Frequency: {characteristic.frequency} <br />
                        Setup Tech: {characteristic.setup_tech} <br />
                        Method: {characteristic.method} <br />
                        Expected Value: {characteristic.expected} <br />
                        Tolerance: {characteristic.tolerance} <br />
                        Form Type: {characteristic.form_type}
                      </Card.Description>
                      <Button negative onClick={this.handleRemove.bind(this, characteristic.name)}>
                        Delete
                      </Button>
                    </Card.Content>
                  </Card>
                )
              })}
            </Card.Group>}
        </Form>
        <Button id='submit' positive onClick={this.handleOpen}>Submit</Button>
        <Confirm
          open={this.state.open}
          onCancel={this.handleClose}
          onConfirm={this.handleSubmit}
        />
      </div>
    )
  }
}
export default Admin
