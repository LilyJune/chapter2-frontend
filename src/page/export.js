import React, { Component } from 'react'
import { Button, Dimmer, Header, Loader, Message } from 'semantic-ui-react'
import DataTable from '../component/data-table'
import DataChart from '../component/data-chart'
import './margins.css'
import ExportNav from '../component/export-nav'

const download = require('downloadjs')
const queryString = require('query-string')

class Export extends Component {
  constructor (props) {
    super(props)

    const parsed = queryString.parse(window.location.search)

    this.state = {
      props: this.props,
      jobNumber: parsed.job_number,
      workOrder: parsed.work_order,
      httpService: this.props.httpService,
      messageService: this.props.messageService,
      data: [],
      res: 'Loading'
    }
    this.handleGetFile = this.handleGetFile.bind(this)
  }

  componentDidMount () {
    if (typeof (this.state.httpService) !== 'undefined') {
      this.state.httpService.getVisualData(window.location.search).then((data) => {
        if (typeof (data) !== 'undefined') {
          this.setState({
            data: data,
            res: 'Success'
          })
        }
      }).catch((err) => {
        this.state.messageService.showMessage(`Data could not be retrieved: ${err}`)
        this.setState({
          res: 'Error'
        })
      })
    }
  }

  handleGetFile () {
    if (typeof (this.state.httpService) !== 'undefined') {
      this.state.httpService.getExportedFiles(window.location.search)
        .then((res) => {
          return res.blob()
        }).then(blob => {
          if (typeof (this.state.jobNumber) !== 'undefined' && typeof (this.state.workOrder) !== 'undefined') {
            download(blob, `Exported_Data_Job_Number_${this.state.jobNumber}_Work_Order_${this.state.workOrder}_${Date.now()}.csv`)
          } else if (typeof (this.state.jobNumber) !== 'undefined') {
            download(blob, `Exported_Data_Job_Number_${this.state.jobNumber}_${Date.now()}.csv`)
          } else {
            download(blob, `Exported_Data_${Date.now()}.zip`)
          }
        }).catch((err) => {
          this.state.messageService.showMessage(`Data could not be retrieved: ${err}`)
        })
    }
  }

  render () {
    if (this.state.res === 'Loading') {
      return (
        <Dimmer active inverted>
          <Loader size='large'>Loading</Loader>
        </Dimmer>
      )
    } else if (this.state.res === 'Error') {
      return (
        <Message
          error
          header='Data could not be retreived!'
        />
      )
    } else if (this.state.res === 'Success') {
      const isData = typeof (this.state.data) !== 'undefined' && this.state.data.length > 0
      return (
        <div id='export' style={{ marginBottom: '2%' }}>
          {isData &&
            <div className='PageLayout'>
              <Header as='h1'>Export Data</Header>
              <ExportNav props={this.state.props} />
              <DataChart data={this.state.data} />
              <div id='export-button'>
                <Button positive onClick={this.handleGetFile}>Export Data As CSV</Button>
              </div>
              <div style={{ overflowX: 'auto', marginTop: '2%' }}>
                <DataTable data={this.state.data} />
              </div>
            </div>}
          {!isData &&
            <div className='PageLayout'>
              <Header as='h1'>Export Data</Header>
              <ExportNav props={this.state.props} />
              <Message
                header='There is no data!'
              />
            </div>}
        </div>
      )
    }
  }
}

export default Export
