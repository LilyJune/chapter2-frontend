import React from 'react'
import { shallow, mount, configure } from 'enzyme'
import Error from './error'
import Adapter from 'enzyme-adapter-react-16'

configure({ adapter: new Adapter() })

describe('<Error />', () => {
  it('should render without crashing', () => {
    shallow(<Error />)
  })

  it('should display an appropriate message by default', () => {
    const wrap = mount(<Error />)
    expect(wrap.text()).toEqual('404 ErrorThe page you are trying to access does not exist')
  })
})
