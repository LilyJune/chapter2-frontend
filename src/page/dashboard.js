import React, { Component } from 'react'
/* import { escapeRegExp, filter } from 'lodash' */
import { Table, Pagination, Dropdown } from 'semantic-ui-react'
import './margins.css'
const queryString = require('query-string')

const options = [
  { key: '1', value: '25', text: '25' },
  { key: '2', value: '50', text: '50' },
  { key: '3', value: '75', text: '75' },
  { key: '4', value: '100', text: '100' }
]

class Dashboard extends Component {
  constructor (props) {
    super(props)
    this.state = {
      httpService: this.props.httpService,
      authService: this.props.authService,
      messageService: this.props.messageService,
      originalData: [],
      /*
      searchResult: [],
      searchValue: '',
      */
      totalPages: 1,
      activePage: 1,
      rowsPerPage: options[0].value,
      columns: [
        'Job Number',
        'Inspection Number',
        'Frequency',
        'Inspection Type',
        'Form Type',
        'Setup Technician',
        'Inspection Method',
        'Expected Result',
        'Tolerance'
      ]
    }
    /*
    this.handleSearch = this.handleSearch.bind(this)
  */
    this.handlePageChange = this.handlePageChange.bind(this)
    this.handleLimChange = this.handleLimChange.bind(this)
  }

  /*
    handleSearch (e, data) {
    this.setState({ searchValue: data.value })

    setTimeout(() => {
      const re = new RegExp('^' + escapeRegExp(this.state.searchValue))
      const isMatch = (result) => re.test(result.jobNumber)

      this.setState({
        searchResult: filter(this.state.originalData, isMatch)
      })
    }, 300)
  } */

  renderTableRows () {
    /*
    let array = this.state.originalData

    if (this.state.searchValue.length >= 1) {
      array = this.state.searchResult
    }
*/
    return this.state.originalData.map((array, index) => {
      const { jobNumber, inspectionNumber, frequency, inspectionType, formType, setupTech, method, expected, tolerance } = array
      return (
        <Table.Row key={index} style={{ textAlign: 'center' }}>
          <Table.Cell>{jobNumber}</Table.Cell>
          <Table.Cell>{inspectionNumber}</Table.Cell>
          <Table.Cell>{frequency}</Table.Cell>
          <Table.Cell>{inspectionType}</Table.Cell>
          <Table.Cell>{formType}</Table.Cell>
          <Table.Cell>{setupTech}</Table.Cell>
          <Table.Cell>{method}</Table.Cell>
          <Table.Cell>{expected}</Table.Cell>
          <Table.Cell>{tolerance}</Table.Cell>
        </Table.Row>
      )
    })
  }

  renderTableHeaders () {
    return this.state.columns.map((colName, index) => {
      return <Table.HeaderCell key={index}> {colName}</Table.HeaderCell>
    })
  }

  getInstructionCountByWorker () {
    if (typeof (this.state.httpService) !== 'undefined') {
      this.state.httpService.getInstructionCountByUserType()
        .then((response) => {
          if (typeof (response) !== 'undefined' && typeof (response.count) !== 'undefined') {
            this.setState({ totalPages: Math.ceil(response.count / this.state.rowsPerPage) })
          }
        })
        .catch((err) => {
          this.state.messageService.showMessage(`Error: ${err}`)
        })
    }
  }

  getInstructionsByWorker () {
    if (typeof (this.state.httpService) !== 'undefined') {
      const params = {}
      const isActivePage = this.state.activePage !== ''
      const isRowsPerPage = this.state.itemsPerPage !== ''

      if (isActivePage && isRowsPerPage) {
        params.active_page = this.state.activePage
        params.rows = this.state.rowsPerPage
      }

      this.state.httpService.getInstructionsByUserType(`?${queryString.stringify(params)}`)
        .then((response) => {
          if (typeof (response) !== 'undefined' && typeof (response.data) !== 'undefined') {
            this.setState({
              originalData: response.data.map((obj) => {
                return {
                  jobNumber: obj.job_number,
                  inspectionNumber: obj.inspection_number,
                  frequency: obj.frequency,
                  inspectionType: obj.inspection_type,
                  formType: obj.form_type,
                  setupTech: obj.setup_tech,
                  method: obj.method,
                  expected: obj.expected,
                  tolerance: obj.tolerance
                }
              })
            })
          }
        })
        .catch((err) => {
          this.state.messageService.showMessage(`Error: ${err}`)
        })
    }
  }

  componentDidMount () {
    this.getInstructionCountByWorker()
    this.getInstructionsByWorker()
  }

  handlePageChange (event, data) {
    const { activePage } = data
    this.setState({ activePage: activePage }, () => {
      this.getInstructionCountByWorker()
      this.getInstructionsByWorker()
    })
  }

  handleLimChange (event, data) {
    if (data.value !== this.rowsPerPage) {
      this.setState({ activePage: 1, rowsPerPage: data.value }, () => {
        this.getInstructionCountByWorker()
        this.getInstructionsByWorker()
      })
    }
  }

  render () {
    return (
      <div className='PageLayout'>
        <h1 id='title' style={{ textAlign: 'center' }}> Instructions Dashboard </h1>
        {/* <Input onChange={this.handleSearch} style={{ float: 'right' }} label='Job Search' /> */}
        <Table striped celled color='red'>

          <Table.Header style={{ textAlign: 'center' }}>
            <Table.Row>
              {this.renderTableHeaders()}
            </Table.Row>
          </Table.Header>

          <Table.Body>
            {this.renderTableRows()}
          </Table.Body>

          <Table.Footer>
            <Table.Row>
              <Table.HeaderCell textAlign='right' colSpan='10'>
                <Dropdown
                  simple
                  style={{ padding: '10px' }}
                  options={options}
                  text={this.state.rowsPerPage.toString()}
                  value={this.state.rowsPerPage}
                  onChange={this.handleLimChange}
                />
                <Pagination
                  activePage={this.state.activePage}
                  totalPages={this.state.totalPages}
                  onPageChange={this.handlePageChange}
                />
              </Table.HeaderCell>
            </Table.Row>
          </Table.Footer>
        </Table>
      </div>
    )
  }
}

export default Dashboard
