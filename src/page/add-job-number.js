import React from 'react'
import { Button, Confirm, Form, Header } from 'semantic-ui-react'
import { Redirect } from 'react-router-dom'
import './margins.css'

class AdminJobAddingForm extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      advancedInstructions: '',
      operation: '',
      partName: '',
      partNumber: 0,
      customer: '',
      jobNumber: '',
      service: this.props.service,
      messageService: this.props.messageService,
      open: false,
      redirect: false
    }
    this.handleOpen = this.handleOpen.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.handleClose = this.handleClose.bind(this)
    this.handleModalSubmitData = this.handleModalSubmitData.bind(this)
  }

  renderRedirect (jobNumber) {
    if (this.state.redirect) {
      return <Redirect to={`/admin/instructions/${jobNumber}`} />
    }
  }

  handleChange (e) {
    if (typeof (e.target.value) !== 'undefined') {
      if (e.target.id === 'advancedInstructionsInput') {
        this.setState({ advancedInstructions: e.target.value.toLowerCase() })
      } else if (e.target.id === 'operationInput') {
        this.setState({ operation: e.target.value.toLowerCase() })
      } else if (e.target.id === 'partNameInput') {
        this.setState({ partName: e.target.value.toLowerCase() })
      } else if (e.target.id === 'partNumberInput') {
        this.setState({ partNumber: parseInt(e.target.value) })
      } else if (e.target.id === 'jobNumberInput') {
        this.setState({ jobNumber: e.target.value.toLowerCase() })
      } else if (e.target.id === 'customerInput') {
        this.setState({ customer: e.target.value.toLowerCase() })
      }
    }
  }

  handleOpen (e) {
    this.setState({ open: true })
  }

  handleClose (e) {
    this.setState({ open: false })
  }

  handleModalSubmitData (e) {
    const temp =
    {
      advanced_instructions: this.state.advancedInstructions,
      operation: this.state.operation,
      part_name: this.state.partName,
      part_number: this.state.partNumber,
      customer: this.state.customer,
      job_number: this.state.jobNumber
    }
    this.state.service.postAddJobNumber(temp)
      .then(() => {
        this.state.messageService.showMessage('Success!!! A job number has been added!')
        this.setState({
          fields: [],
          open: false,
          redirect: true
        })
      })
      .catch((err) => {
        this.state.messageService.showMessage(`Error: ${err}`)
        this.setState({ open: false })
      })
  }

  render () {
    return (
      <div className='PageLayout' text onChange={this.handleChange}>
        <Header as='h2'>Add Job Number</Header>
        <Form className='attached fluid segment'>
          <Form.Field>
            <Header as='h5'>Job Number</Header>
            <Form.Input id='jobNumberInput' placeholder='Job Number' />
          </Form.Field>
          <Form.Field>
            <Header as='h5'>Advanced Instructions</Header>
            <Form.Input id='advancedInstructionsInput' placeholder='Link Adv. Instructions' />
          </Form.Field>
          <Form.Field>
            <Header as='h5'>Operation</Header>
            <Form.Input id='operationInput' placeholder='Operation' />
          </Form.Field>
          <Form.Field>
            <Header as='h5'>Part Name</Header>
            <Form.Input id='partNameInput' placeholder='Part Name' />
          </Form.Field>
          <Form.Field>
            <Header as='h5'>Customer</Header>
            <Form.Input id='customerInput' placeholder='Customer' />
          </Form.Field>
          <Form.Field>
            <Header as='h5'>Part Number</Header>
            <Form.Input id='partNumberInput' placeholder='Part Number' />
          </Form.Field>
          <Button id='submitButton' type='submit' onClick={this.handleOpen}>Submit</Button>
        </Form>
        <Confirm
          id='buttonConfirm'
          cancelButton='Cancel'
          confirmButton='Submit'
          content='Are you sure you want to submit add this job number?'
          open={this.state.open}
          onCancel={this.handleClose}
          onConfirm={this.handleModalSubmitData}
        />
        {this.renderRedirect(this.state.jobNumber)}
      </div>
    )
  }
}
export default AdminJobAddingForm
