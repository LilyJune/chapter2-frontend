import React, { Component } from 'react'
import { Header, Segment, Table } from 'semantic-ui-react'

class ToDoList extends Component {
  constructor (props) {
    super(props)
    this.state = {
      authService: this.props.authService,
      httpService: this.props.httpService,
      messageService: this.props.messageService,
      data: []
    }
    this.renderData = this.renderData.bind(this)
  }

  componentDidMount () {
    if (typeof (this.state.httpService) !== 'undefined') {
      this.state.httpService.getUserAssignments()
        .then((data) => {
          if (typeof (data) !== 'undefined') {
            this.setState({
              data: data
            })
          }
        }).catch((err) => {
          this.state.messageService.showMessage(`Data could not be retrieved: ${err}`)
        })
    }
  }

  renderData () {
    if (this.state.data.length > 0) {
      return this.state.data.map((items) => {
        return (
          <Table.Row key={items.id}>
            <Table.Cell>{items.id}</Table.Cell>
            <Table.Cell>{items.inspection_number}</Table.Cell>
            <Table.Cell textAlign='right'>{items.job_number}</Table.Cell>
          </Table.Row>
        )
      }
      )
    } else {
      return (
        null
      )
    }
  }

  render () {
    return (
      <div>
        <h1 id='title' style={{ textAlign: 'center' }}> To Do List: </h1>
        <Segment attached fluid='true'>
          <Header as='h3'>My Assignments</Header>
          <Table celled striped>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell singleLine>Id</Table.HeaderCell>
                <Table.HeaderCell>Inspection Number</Table.HeaderCell>
                <Table.HeaderCell>Job Number</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>
              {this.renderData()}
            </Table.Body>
          </Table>
        </Segment>
      </div>
    )
  }
}

export default ToDoList
