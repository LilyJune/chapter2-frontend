import React, { Component } from 'react'
import { Form, Header, Confirm, Label } from 'semantic-ui-react'
import './margins.css'

const find = require('lodash.find')
const map = require('lodash.map')

const shiftOptions = [
  { key: '1', text: '1', value: '1' },
  { key: '2', text: '2', value: '2' },
  { key: '3', text: '3', value: '3' }
]

class Inspection extends Component {
  constructor (props) {
    super(props)
    this.state = {
      jobNumber: this.props.jobNumber,
      workOrder: this.props.workOrder,
      service: this.props.service,
      authService: this.props.authService,
      messageService: this.props.messageService,
      inspectionType: this.props.inspectionType,
      open: false,
      data: [],
      characteristicData: {},
      allCharacteristics: [],
      charcteristic: '',
      machine: '',
      shift: '',
      result: '',
      remark: ''
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleCharacteristicChange = this.handleCharacteristicChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleOpen = this.handleOpen.bind(this)
    this.handleClose = this.handleClose.bind(this)
    this.handleCheckboxClick = this.handleCheckboxClick.bind(this)
  }

  handleChange (e, { name, value }) {
    this.setState({ [name]: value })
  }

  handleCharacteristicChange (e, { value }) {
    const characteristicData = find(this.state.data, { name: value })
    if (characteristicData.form_type === 'Checkbox') {
      this.setState({
        characteristicData: characteristicData,
        result: false,
        characteristic: value,
        inspectionNumber: characteristicData.inspection_number
      })
    } else {
      this.setState({
        characteristicData: characteristicData,
        result: '',
        characteristic: value,
        inspectionNumber: characteristicData.inspection_number
      })
    }
  }

  handleCheckboxClick () {
    const opposite = !this.state.result
    this.setState({
      result: opposite
    })
  }

  handleSubmit () {
    const dataToSubmit = {
      job_number: this.state.jobNumber,
      work_order: this.state.workOrder,
      inspection_type: this.state.inspectionType,
      inspection_number: this.state.inspectionNumber,
      machine: this.state.machine,
      shift: this.state.shift,
      operator: this.state.authService.getID(),
      characteristic: this.state.characteristic,
      result: this.state.result,
      remark: this.state.remark
    }

    this.state.service.postInspection(this.state.jobNumber, this.state.workOrder, dataToSubmit)
      .then(() => {
        this.setState({
          open: false,
          charcteristic: '',
          machine: '',
          shift: '',
          result: '',
          remark: ''
        })

        this.state.messageService.showMessage(`Your Inspection for job number: ${this.state.jobNumber} has submitted sucessfully`)
      })
      .catch((err) => {
        this.state.messageService.showMessage(`Error: ${err}`)
      })
  }

  handleClose () {
    this.setState({ open: false })
  }

  handleOpen () {
    this.setState({ open: true })
  }

  componentDidMount () {
    if (typeof (this.state.service) !== 'undefined') {
      this.state.service.getCustomForm(this.state.jobNumber, this.state.workOrder).then((data) => {
        if (typeof (data) !== 'undefined') {
          const allCharacteristics = map(data.characteristics, 'name')
          const allCharacteristicsFormated = []
          for (let i = 0; i < allCharacteristics.length; i++) {
            allCharacteristicsFormated.push(
              {
                key: i + 1,
                text: allCharacteristics[i],
                value: allCharacteristics[i],
                inspectionNumber: data.characteristics[i].inspection_number
              })
          }
          this.setState({
            data: data.characteristics,
            allCharacteristics: allCharacteristicsFormated
          })
        }
      }).catch((err) => {
        this.state.messageService.showMessage(`Error: ${err}`)
      })
    }
  }

  render () {
    if (typeof (this.state.data) !== 'undefined' && this.state.data.length > 0) {
      return (
        <div className='PageLayout' id='inspection'>
          <Header as='h1'>Job Number: {this.state.jobNumber}    Work Order: {this.state.workOrder}</Header>
          <Form onSubmit={this.handleOpen}>
            <Form.Input required label='machine' onChange={this.handleChange} name='machine' value={this.state.machine} />
            <Form.Select required label='shift' onChange={this.handleChange} options={shiftOptions} name='shift' value={this.state.shift} />
            <Form.Select required label='characteristic' onChange={this.handleCharacteristicChange} options={this.state.allCharacteristics} name='characteristic' />
            {this.state.characteristicData !== {} &&
              <div>
                <Label>
                  {this.state.characteristicData.method}
                </Label>
                <Label>
                  {this.state.characteristicData.frequency}
                </Label>
                {this.state.characteristicData.form_type === 'Input' &&
                  <Form.Input
                    required
                    label='result'
                    onChange={this.handleChange}
                    name='result'
                    value={this.state.result}
                  />}
                {this.state.characteristicData.form_type === 'Checkbox' &&
                  <Form.Checkbox
                    required
                    label='result'
                    onChange={this.handleCheckboxClick}
                    name='result'
                  />}
                <Form.TextArea
                  label='remark'
                  onChange={this.handleChange}
                  name='remark'
                  value={this.state.remark}
                />
              </div>}
            <Form.Button id='submit'>
              Submit
            </Form.Button>
          </Form>
          <Confirm
            open={this.state.open}
            onCancel={this.handleClose}
            onConfirm={this.handleSubmit}
          />
        </div>
      )
    } else {
      return (
        <div id='inspection'>
          <Header as='h1'>Job Number and Work Order Combination Not Found!</Header>
        </div>
      )
    }
  }
}

export default Inspection
