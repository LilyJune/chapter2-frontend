import React from 'react'
import { shallow, configure } from 'enzyme'
import Home from './home'
import Adapter from 'enzyme-adapter-react-16'

configure({ adapter: new Adapter() })

describe('<Home />', () => {
  it('should render without crashing', () => {
    shallow(<Home />)
  })

  it('should display an appropriate message by default', () => {
    const wrap = shallow(<Home />)
    expect(wrap.find('Message')).toHaveLength(1)
  })
})
