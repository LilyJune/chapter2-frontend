import React from 'react'
import { shallow, mount, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import Export from './export'
import MockService from '../../test-assets/mock-service'
import MessageService from '../service/message-service'

const messageService = new MessageService()

configure({ adapter: new Adapter() })

describe('<Export />', () => {
  it('should render without crashing', () => {
    shallow(<Export messageService={messageService} />)
  })

  it('should render without crashing given jobNumber', () => {
    shallow(<Export jobNumber='testJobNumber' messageService={messageService} />)
  })

  it('should render without crashing given jobNumber and workOrder', () => {
    shallow(<Export jobNumber='testJobNumber' workOrder='testWorkOrder' messageService={messageService} />)
  })

  it('should render without crashing given a service to rerender with', () => {
    mount(<Export httpService={new MockService()} messageService={messageService} />)
  })

  it('should render without crashing given a service to rerender with even if it get undefined data', () => {
    mount(<Export httpService={new MockService()} jobNumber='testJobNumber' messageService={messageService} />)
  })

  it('should render handle errors when fetching data', () => {
    mount(<Export httpService={new MockService()} jobNumber='testJobNumber' workOrder='testWorkOrder' messageService={messageService} />)
  })
})
