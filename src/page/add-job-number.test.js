import React from 'react'
import { shallow, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import AddJobNumberForm from './add-job-number'
import MessageService from '../service/message-service'

const messageService = new MessageService()

configure({ adapter: new Adapter() })

describe('<AddJobNumberForm />', () => {
  it('should render without crashing', () => {
    shallow(<AddJobNumberForm messageService={messageService} />)
  })
  it('should render all input boxes with correct names', () => {
    const wrap = (shallow(<AddJobNumberForm messageService={messageService} />))
    expect(wrap.find('#advancedInstructionsInput').props('value').placeholder).toEqual('Link Adv. Instructions')
    expect(wrap.find('#operationInput').props('value').placeholder).toEqual('Operation')
    expect(wrap.find('#partNumberInput').props('value').placeholder).toEqual('Part Number')
    expect(wrap.find('#partNameInput').props('value').placeholder).toEqual('Part Name')
    expect(wrap.find('#customerInput').props('value').placeholder).toEqual('Customer')
    expect(wrap.find('#jobNumberInput').props('value').placeholder).toEqual('Job Number')
    expect(wrap.find('#submitButton').props('value').children).toEqual('Submit')
  })

  it('should update when new value is passed in', () => {
    const wrap = (shallow(<AddJobNumberForm messageService={messageService} />))
    const advInstructionsInputTest = wrap.find('#advancedInstructionsInput')
    advInstructionsInputTest.instance.value = 'www.google.com'
    advInstructionsInputTest.simulate('change')
    expect(advInstructionsInputTest.instance.value).toEqual('www.google.com')

    const operationInputTest = wrap.find('#operationInput')
    operationInputTest.instance.value = '511'
    operationInputTest.simulate('change')
    expect(operationInputTest.instance.value).toEqual('511')

    const jobNumberInputTest = wrap.find('#jobNumberInput')
    jobNumberInputTest.instance.value = '123'
    jobNumberInputTest.simulate('change')
    expect(jobNumberInputTest.instance.value).toEqual('123')

    const partNumberInputTest = wrap.find('#partNumberInput')
    partNumberInputTest.instance.value = 4455
    partNumberInputTest.simulate('change')
    expect(partNumberInputTest.instance.value).toEqual(4455)

    const partNameInputTest = wrap.find('#partNameInput')
    partNameInputTest.instance.value = 'Favorite Part'
    partNameInputTest.simulate('change')
    expect(partNameInputTest.instance.value).toEqual('Favorite Part')

    const customerInputTest = wrap.find('#customerInput')
    customerInputTest.instance.value = 'customer'
    customerInputTest.simulate('change')
    expect(customerInputTest.instance.value).toEqual('customer')
  })
})
