import React, { Component } from 'react'
import TCS from '../assets/TCS.png'
import Chapter2 from '../assets/favicon-96x96.png'
import { Header, Grid, Image, Message, Segment } from 'semantic-ui-react'
import './margins.css'
import Dashboard from './dashboard'
import InspectionNavigation from '../component/inspection-navigation'
import InstructionNavigation from '../component/instruction-navigation'
import AssignmentDashboard from './assignment-dashboard'
import ToDoList from './to-do-list'

class Home extends Component {
  constructor (props) {
    super(props)
    this.state = {
      authService: this.props.authService,
      httpService: this.props.httpService,
      messageService: this.props.messageService
    }
  }

  render () {
    if (typeof (this.state.authService) !== 'undefined' && (this.state.authService.getLevel() === 3)) {
      return (
        <div>
          <InstructionNavigation authService={this.state.authService} />
          <InspectionNavigation authService={this.state.authService} />
          <AssignmentDashboard {...this.props} />
          <Dashboard {...this.props} />
          <ToDoList {...this.props} />
        </div>
      )
    } else if (typeof (this.state.authService) !== 'undefined' && (this.state.authService.getLevel() >= 1)) {
      return (
        <div>
          <InspectionNavigation authService={this.state.authService} />
          <AssignmentDashboard {...this.props} />
          <Dashboard {...this.props} />
          <ToDoList {...this.props} />
        </div>
      )
    } else {
      return (
        <div className='PageLayout'>
          <Header as='h2'>Welcome to the Project!</Header>
          <Segment attached fluid='true'>
            <Header as='h3'>Partnership</Header>
            <Grid centered columns='2'>
              <Grid.Row columns={2}>
                <Grid.Column>
                  <Header as='h5'>Development Team 'TCS' Logo:</Header>
                  <Image src={TCS} size='small' />
                </Grid.Column>
                <Grid.Column>
                  <Header as='h5'>Product Owner 'Chapter 2, Inc.' Logo:</Header>
                  <Image src={Chapter2} size='small' />
                </Grid.Column>
              </Grid.Row>
            </Grid>
            <Message
              attached
              header='Background'
              content='Hello we are TCS. This stands for: Tired College Students. The app was created for Chapter 2 as our groups senior design project for Milwaukee School of Engineering. The project aims to replace and improve the functionality that was previously had in the existing logging service "Paperless Inspection Form". The TCS team memebers are: Donal Moloney, Max Beihoff, Lily Lux, Trey Gallun, Steve Wasielewski. We very much enjoyed working on the project for you.'
            />
          </Segment>
        </div>
      )
    }
  }
}

export default Home
