import React, { Component } from 'react'
import { Form, Header, Grid, Button, Checkbox } from 'semantic-ui-react'
import './margins.css'

const formOptions = [
  {
    key: 'Operator',
    text: 'Operator',
    value: '1'
  },
  {
    key: 'Inspector',
    text: 'Inspector',
    value: '2'
  },
  {
    key: 'Admin',
    text: 'Admin',
    value: '3'
  }
]

class EditUserRights extends Component {
  constructor (props) {
    super(props)
    this.state = {
      usernameToUpdate: '',
      updatePassword: '',
      updateAccountType: '1',
      newAccountUserName: '',
      newAccountUsername: '',
      newAccountPassword: '',
      newAccountId: '',
      newAccountType: '1',
      authService: this.props.authService,
      messageService: this.props.messageService,
      service: this.props.service,
      togglePassword: false,
      togglePrivelage: false
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmitUpdateAccount = this.handleSubmitUpdateAccount.bind(this)
    this.handleSubmitNewAccount = this.handleSubmitNewAccount.bind(this)
    this.handleSelect = this.handleSelect.bind(this)
    this.handleTogglePassword = this.handleTogglePassword.bind(this)
    this.handleTogglePrivileges = this.handleTogglePrivileges.bind(this)
  }

  handleChange (e, { name, value }) {
    this.setState({ [name]: value })
  }

  handleTogglePassword (e) {
    if (this.state.togglePassword === true) {
      this.setState({ togglePassword: false })
    } else {
      this.setState({ togglePassword: true })
      this.setState({ updatePassword: '' })
    }
  }

  handleTogglePrivileges (e) {
    if (this.state.togglePrivelage === true) {
      this.setState({ togglePrivelage: false })
    } else {
      this.setState({ togglePrivelage: true })
      this.setState({ updateAccountType: '1' })
    }
  }

  handleSubmitUpdateAccount (e) {
    if (this.state.usernameToUpdate === null || (this.state.togglePassword !== true && this.state.togglePrivelage !== true)) {
      this.state.messageService.showMessage('Error: The username, password or admin role has to be filled. Please fill!')
    } else {
      var data = {
        username: String(this.state.usernameToUpdate)
      }
      if (this.state.togglePassword === true) {
        data.password = this.state.updatePassword
      }
      if (this.state.togglePrivelage === true) {
        data.permission = this.state.updateAccountType
      }
      this.state.service.putUpdateCreateAccount(data)
        .then(() => {
          this.state.messageService.showMessage('Success you have modified the account!')
        })
        .catch((err) => {
          this.state.messageService.showMessage(`Error: ${err}`)
        })
    }
  }

  handleSubmitNewAccount (e) {
    if (this.state.newAccountUsername === '' || this.state.newAccountUserName === '' ||
    this.state.newAccountPassword === '' || this.state.newAccountId === '' || this.state.newAccountType === '') {
      this.state.messageService.showMessage('Error: Please fill in all boxes for creating a new user')
    } else {
      const data = {
        password: String(this.state.newAccountPassword),
        permission: parseInt(this.state.newAccountType),
        id: parseInt(this.state.newAccountId),
        name: String(this.state.newAccountUserName),
        username: this.state.newAccountUsername,
        function: 'create'
      }
      this.state.service.putUpdateCreateAccount(data)
        .then(() => {
          this.state.messageService.showMessage('Success! A new account was made!')
          this.setState({ newAccountUserName: '', newAccountUsername: '', newAccountPassword: '', newAccountId: '', newAccountType: '1' })
        })
        .catch((err) => {
          this.state.messageService.showMessage(`Error: ${err}`)
        })
    }
  }

  handleSelect (e, formSelect) {
    this.setState({ [formSelect.id]: formSelect.value })
  }

  render () {
    return (
      <div className='PageLayout'>
        <Header id='headerTitle' as='h2'>Edit User Rights</Header>
        <Form className='attached fluid segment'>
          <Form.Field>
            <Header as='h3'>Edit Account</Header>
            <Grid centered columns='2'>
              <Grid.Row columns={1}>
                <Grid.Column>
                  <Form.Input id='usernameInput' onChange={this.handleChange} value={this.state.usernameToUpdate} name='usernameToUpdate' placeholder='Username' label='Username:' />
                </Grid.Column>
                <Grid.Column>
                  <Form.Input id='passwordInput' onChange={this.handleChange} disabled={this.state.togglePassword === false} value={this.state.updatePassword} name='updatePassword' placeholder='Password' label='Password:' />
                  <Checkbox id='checkboxPassword' label='Change Password' checked={this.state.togglePassword} onChange={this.handleTogglePassword} />
                </Grid.Column>
                <Grid.Column>
                  <Form.Select
                    required
                    disabled={this.state.togglePrivelage === false}
                    id='updateAccountType'
                    onChange={this.handleSelect}
                    placeholder='Role Type'
                    label='Role Type'
                    options={formOptions}
                    value={this.state.updateAccountType}
                  />
                  <Checkbox label='Change Privilege' checked={this.state.togglePrivelage} onChange={this.handleTogglePrivileges} />
                </Grid.Column>
                <Grid.Row columns={1}>
                  <Button id='buttonUdate' positive onClick={this.handleSubmitUpdateAccount}>Update Account</Button>
                </Grid.Row>
              </Grid.Row>
            </Grid>
          </Form.Field>
          <Form.Field>
            <Header as='h3'>Create New Account</Header>
            <Grid centered columns='2'>
              <Grid.Row columns={1}>
                <Grid.Column>
                  <Form.Input id='userOfficialNameInput' onChange={this.handleChange} value={this.state.newAccountUserName} name='newAccountUserName' placeholder='User Official Name' label='User Official Name:' />
                </Grid.Column>
                <Grid.Column>
                  <Form.Input id='usernameInput' onChange={this.handleChange} value={this.state.newAccountUsername} name='newAccountUsername' placeholder='Username' label='Username:' />
                </Grid.Column>
                <Grid.Column>
                  <Form.Input id='idInput' onChange={this.handleChange} value={this.state.newAccountId} name='newAccountId' placeholder='Id' label='ID:' />
                </Grid.Column>
                <Grid.Column>
                  <Form.Input id='passwordInput' onChange={this.handleChange} value={this.state.newAccountPassword} name='newAccountPassword' placeholder='Password' label='Password:' />
                </Grid.Column>
                <Grid.Column>
                  <Form.Select
                    id='newAccountType'
                    onChange={this.handleSelect}
                    placeholder='Role Type'
                    label='Role Type'
                    options={formOptions}
                    value={this.state.newAccountType}
                  />
                </Grid.Column>
                <Grid.Row columns={1}>
                  <Button id='submitNewUser' positive onClick={this.handleSubmitNewAccount}>Create New Account</Button>
                </Grid.Row>
              </Grid.Row>
            </Grid>
          </Form.Field>
        </Form>
      </div>
    )
  }
}

export default EditUserRights
