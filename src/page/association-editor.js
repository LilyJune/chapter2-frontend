import React, { Component } from 'react'
import { Button, Card, Confirm, Form, Header, Divider } from 'semantic-ui-react'
import './margins.css'

class WorkOrderJobNumberAssociation extends Component {
  constructor (props) {
    super(props)
    this.state = {
      service: this.props.service,
      messageService: this.props.messageService,
      jobNumber: '',
      jobNumbers: [],
      workOrder: this.props.work_order,
      associationExists: false
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleAdd = this.handleAdd.bind(this)
    this.handleExist = this.handleExist.bind(this)
    this.handleClose = this.handleClose.bind(this)
    this.handleRemove = this.handleRemove.bind(this)
    this.handleClose = this.handleClose.bind(this)
    this.handleModalCancel = this.handleModalCancel.bind(this)
    this.handleModalSubmitData = this.handleModalSubmitData.bind(this)
    this.handleWorkOrderChange = this.handleWorkOrderChange.bind(this)
    this.handleOpen = this.handleOpen.bind(this)
  }

  handleExist () {
    this.state.service.getWorkOrderJobNumberRelationship(parseInt(this.state.workOrder))
      .then((data) => { this.setState({ associationExists: ((data.job_numbers.length !== 0) && (data.job_numbers.includes(this.state.jobNumber) === true)) }) })
      .catch((err) => {
        this.state.messageService.showMessage(`An error occured when you attempted to get the relationship: ${err}`)
        this.setState(associationExists => false)
      })
  }

  handleModalCancel (e) {
    this.setState({ open: false })
  }

  handleModalSubmitData (e) {
    const sendJSON = { work_order: parseInt(this.state.workOrder), job_numbers: this.state.jobNumbers }
    this.state.service.putWorkOrderJobNumberRelationship(parseInt(this.state.workOrder), sendJSON)
      .then((data) => {
        this.state.messageService.showMessage('Succesfully Associated Work Orders and Job Numbers')
        this.setState({ jobNumbers: [] })
        this.setState({ open: false })
        this.setState({ jobNumber: '' })
        this.setState({ workOrder: '' })
      })
      .catch((err) => {
        this.state.messageService.showMessage(`Could not Succesully Create the Association/s: ${err}`)
      })
  }

  handleChange (e, { value }) {
    this.setState({ jobNumber: String(value) })
  }

  handleRemove (nameOfJobNumber) {
    let temp = this.state.jobNumbers
    const index = temp.indexOf(nameOfJobNumber)
    temp = (index === 0) ? temp.splice(index, index + 1) : temp.splice(index, index)
    this.state.messageService.showMessage(`Job Number ${nameOfJobNumber} nameOfJobNumber Removed`)
    this.setState(jobNumbers => temp)
  }

  handleOpen () {
    this.setState({ open: true })
  }

  handleClose () {
    this.setState({ open: false })
  }

  handleWorkOrderChange (e, { value }) {
    this.setState({ workOrder: String(value) })
  }

  handleAdd () {
    this.handleExist()
    if (this.state.associationExists === false) {
      this.setState(prevState => ({
        jobNumbers: [...prevState.jobNumbers, this.state.jobNumber]
      }))
      this.setState({
        length: this.state.length + 1
      })
    } else {
      this.state.messageService.showMessage('Err: Job Number Already Exist')
      this.setState(
        jobNumber => ''
      )
    }
  }

  render () {
    return (
      <div className='PageLayout' id='admin'>
        <Header as='h2'>Associate Job Number to Work Order Number: </Header>
        <Form className='attached fluid segment'>
          <Form.Input required id='workOrderInput' onChange={this.handleWorkOrderChange} value={this.state.workOrder} name='workOrder' placeholder='Work Order' label='Work Order' />
          <Form.Input required id='jobNumberInput' onChange={this.handleChange} value={this.state.jobNumber} name='jobNumber' placeholder='Job Number' label='Job Number' />
          <Form.Button primary id='buttonAdd' onClick={this.handleAdd}>Add </Form.Button>
        </Form>
        {this.state.jobNumbers.length > 0 &&
          <Card.Group>
            {this.state.jobNumbers.map((jobNumber, i) => {
              return (
                <Card key={i}>
                  <Card.Content>
                    <Card.Description>
                      Job Number:{jobNumber} <br />
                      Work Order:{this.state.workOrder} <br />
                    </Card.Description>
                    <Button negative onClick={this.handleRemove.bind(this, jobNumber)}>
                      Delete
                    </Button>
                  </Card.Content>
                </Card>
              )
            })}
          </Card.Group>}
        <Divider />
        <Button id='submit' positive onClick={this.handleOpen}>Submit</Button>
        <Confirm
          id='modelConfirmSubmit'
          cancelButton='Cancel'
          confirmButton='Submit'
          content='Are you sure you want to create the relationship between the job number & work order?'
          open={this.state.open}
          onCancel={this.handleModalCancel}
          onConfirm={this.handleModalSubmitData}
        />
      </div>
    )
  }
}
export default WorkOrderJobNumberAssociation
