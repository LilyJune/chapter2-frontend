import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import { Container, Form, Segment } from 'semantic-ui-react'

class Login extends Component {
  constructor (props) {
    super(props)
    this.state = {
      authService: this.props.authService,
      messageService: this.props.messageService,
      username: '',
      password: ''
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleChange (e, { name, value }) {
    this.setState({ [name]: value })
  }

  handleSubmit () {
    if (typeof (this.state.authService) !== 'undefined') {
      this.state.authService.login(this.state.username, this.state.password)
        .catch((err) => {
          this.state.messageService.showMessage(`Error: ${err}`)
        })
    } else {
      this.state.messageService.showMessage('Oops! We were unable to send the login request.')
    }
  }

  renderRedirect () {
    if (typeof (this.state.authService) !== 'undefined' && this.state.authService.loggedIn()) {
      return <Redirect to='/' />
    }
  }

  render () {
    return (
      <Container>
        {this.renderRedirect()}
        <Segment>
          <Form onSubmit={this.handleSubmit}>
            <Form.Input required onChange={this.handleChange} value={this.state.username} name='username' placeholder='Username' label='Username' />
            <Form.Input required onChange={this.handleChange} value={this.state.password} name='password' placeholder='Password' label='Password' type='password' />
            <Form.Button>
              Submit
            </Form.Button>
          </Form>
        </Segment>
      </Container>
    )
  }
}

export default Login
