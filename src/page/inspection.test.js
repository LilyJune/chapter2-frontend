import React from 'react'
import { shallow, mount, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import Inspection from './inspection'
import MockService from '../../test-assets/mock-service'
import MessageService from '../service/message-service'

const messageService = new MessageService()
const mockService = new MockService()

configure({ adapter: new Adapter() })

const testCharacteristicOne = {
  name: 'test1name',
  method: 'test1method',
  expected: '1',
  tolerance: '0.01',
  form_type: 'Input',
  frequency: 'test1frequency'
}
const testData = [
  testCharacteristicOne,
  {
    name: 'test2name',
    method: 'test2method',
    expected: '2',
    tolerance: '0.02',
    form_type: 'Checkbox',
    frequency: 'test2frequency'
  },
  {
    name: 'test3name',
    method: 'test3method',
    expected: '3',
    tolerance: '0.03',
    form_type: 'Input',
    frequency: 'test3frequency'
  }]

describe('<Inspection />', () => {
  it('should render without crashing', () => {
    shallow(<Inspection messageService={messageService} />)
  })

  it('should not display a form by default', () => {
    const wrap = shallow(<Inspection messageService={messageService} />)
    expect(wrap.find('Form')).toHaveLength(0)
  })

  it('should display an appropriate message by default', () => {
    const wrap = shallow(<Inspection messageService={messageService} />)
    expect(wrap.find('Header').props().children).toEqual('Job Number and Work Order Combination Not Found!')
  })

  it('should render without crashing given a service', () => {
    shallow(<Inspection jobNumber={0} workOrder={0} service={mockService} messageService={messageService} />)
  })

  it('should render a form when given a service', () => {
    const wrap = mount(<Inspection jobNumber={0} workOrder={0} service={mockService} messageService={messageService} />)
    wrap.setState({ data: testData })
    expect(wrap.find('form')).toHaveLength(1)
  })

  it('should display the job number', () => {
    const wrap = mount(<Inspection jobNumber={0} workOrder={0} service={mockService} messageService={messageService} />)
    wrap.setState({ data: testData })
    expect(wrap.text()).toContain('Job Number: 0')
  })

  it('should display the work order', () => {
    const wrap = mount(<Inspection jobNumber={0} workOrder={0} service={mockService} messageService={messageService} />)
    wrap.setState({ data: testData })
    expect(wrap.text()).toContain('Work Order: 0')
  })

  it('should be able update the characteristic if it has data', () => {
    const wrap = mount(<Inspection jobNumber={0} workOrder={0} service={mockService} messageService={messageService} />)
    wrap.setState({
      data: testData
    })
    wrap.instance().handleCharacteristicChange(null, { value: 'test1name' })
    expect(wrap.state('characteristicData')).toEqual(testCharacteristicOne)
  })

  it('should update data upon mount', async (done) => {
    const wrap = mount(<Inspection jobNumber={0} workOrder={0} service={mockService} messageService={messageService} />)
    setTimeout(() => {
      expect(wrap.state('data')).toEqual(testData)
      done()
    }, 3000)
  })

  it('should set the charactertistic state to the characteristic name', () => {
    const wrap = mount(<Inspection jobNumber={0} workOrder={0} service={mockService} messageService={messageService} />)
    wrap.setState({
      data: testData
    })
    wrap.instance().handleCharacteristicChange(null, { value: 'test1name' })
    expect(wrap.state('characteristic')).toEqual('test1name')
  })

  it('should set the result to false when it renders a checkbox', () => {
    const wrap = mount(<Inspection jobNumber={0} workOrder={0} service={mockService} messageService={messageService} />)
    wrap.setState({
      data: testData
    })
    wrap.instance().handleCharacteristicChange(null, { value: 'test2name' })
    expect(wrap.state('result')).toBe(false)
  })

  it('should set result to true when the Checkbox is clicked', () => {
    const wrap = mount(<Inspection jobNumber={0} workOrder={0} service={mockService} messageService={messageService} />)
    wrap.setState({
      data: testData
    })
    wrap.instance().handleCharacteristicChange(null, { value: 'test2name' })
    wrap.instance().handleCheckboxClick()
    expect(wrap.state('result')).toBe(true)
  })

  it('should not submit unless required fields are filled', () => {
    const wrap = mount(<Inspection jobNumber={0} workOrder={0} service={mockService} messageService={messageService} />)
    wrap.setState({
      data: testData
    })
    const submitButton = wrap.find('#submit').at(0)
    submitButton.simulate('click')
    expect(wrap.state('open')).toBe(false)
  })
})
