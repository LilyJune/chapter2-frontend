import React, { Component } from 'react'
import { Dimmer, Header, Loader, Message } from 'semantic-ui-react'
import { VictoryChart, VictoryAxis, VictoryLine, VictoryTooltip, VictoryVoronoiContainer, VictoryGroup, VictoryScatter } from 'victory'
import './margins.css'
import ExportNav from '../component/export-nav'

const queryString = require('query-string')
const groupBy = require('lodash.groupby')

class PercentError extends Component {
  constructor (props) {
    super(props)

    const parsed = queryString.parse(window.location.search)

    this.state = {
      jobNumber: parsed.job_number,
      httpService: this.props.httpService,
      messageService: this.props.messageService,
      data: {},
      res: 'Loading'
    }
    this.formatData = this.formatData.bind(this)
    this.formatCharts = this.formatCharts.bind(this)
  }

  componentDidMount () {
    if (typeof (this.state.httpService) !== 'undefined') {
      this.state.httpService.getVisualData(window.location.search).then((data) => {
        if (typeof (data) !== 'undefined') {
          console.log(data)
          data = this.formatData(data)
          this.setState({
            data: data,
            res: 'Success'
          })
        }
      }).catch((err) => {
        this.state.messageService.showMessage(`Data could not be retrieved: ${err}`)
        this.setState({
          res: 'Error'
        })
      })
    }
  }

  formatData (data) {
    data = groupBy(data, 'inspection_number')
    const processedData = {}
    for (const [key, value] of Object.entries(data)) {
      const entryArray = []
      for (let i = 0; i < value.length; i++) {
        value[i].x = new Date(value[i].date)
        value[i].y = (100.0 * Math.abs(value[i].expected - value[i].result) / value[i].expected)

        if (!isNaN(value[i].y)) {
          entryArray.push(value)
        }
      }
      if (entryArray.length > 0) {
        processedData[key] = entryArray
      }
    }
    return processedData
  }

  formatCharts () {
    const charts = []
    for (const [key, value] of Object.entries(this.state.data)) { // {inspectionNumber: [{}, {}, {}] }
      charts.push(
        <div key={key}>
          <Header as='h2'>Inspection Number: {key}</Header>
          <VictoryChart
            height={200}
            containerComponent={<VictoryVoronoiContainer />}
          >
            <VictoryGroup
              key={key}
              color='#EE1111'
              labels={({ datum }) => `Percent Error: ${Math.round((datum.y + Number.EPSILON) * 100) / 100}%\nDate: ${datum.x}`}
              labelComponent={
                <VictoryTooltip
                  style={{ fontSize: 6 }}
                />
              }
              data={value[0]}
            >
              <VictoryAxis
                scale='time'
                standalone={false}
                tickFormat={date => date.toLocaleString('en-us', { month: 'short' })}
                fixLabelOverlap
              />
              <VictoryLine />
              <VictoryScatter />
            </VictoryGroup>
          </VictoryChart>
        </div>)
    }
    return charts
  }

  render () {
    if (this.state.res === 'Loading') {
      return (
        <Dimmer active inverted>
          <Loader size='large'>Loading</Loader>
        </Dimmer>
      )
    } else if (this.state.res === 'Error') {
      return (
        <Message
          error
          header='Data could not be retreived!'
        />
      )
    } else if (this.state.res === 'Success') {
      const isData = typeof (this.state.data) !== 'undefined' && Object.keys(this.state.data).length > 0
      return (
        <div id='export' style={{ marginBottom: '2%' }}>
          {isData &&
            <div className='PageLayout'>
              <ExportNav props={this.state.props} />
              <Header as='h1'>Percent Error Charts</Header>
              {this.formatCharts()}
            </div>}
          {!isData &&
            <div className='PageLayout'>
              <Header as='h1'>Percent Error Charts</Header>
              <ExportNav props={this.state.props} />
              <Message
                header='There is no data!'
              />
            </div>}
        </div>
      )
    }
  }
}

export default PercentError
