import React from 'react'
import { shallow, configure } from 'enzyme'
import ViewInspectionHistory from './home'
import Adapter from 'enzyme-adapter-react-16'
import '../service/auth-service'

configure({ adapter: new Adapter() })

describe('<ViewInspectionHistory />', () => {
  it('should render without crashing', () => {
    shallow(<ViewInspectionHistory />)
  })
})
