import React from 'react'
import { shallow, configure } from 'enzyme'
import EditUserRights from './edit-user-rights'
import Adapter from 'enzyme-adapter-react-16'
import MessageService from '../service/message-service'

const messageService = new MessageService()

configure({ adapter: new Adapter() })

describe('<EditUserRights />', () => {
  it('should render without crashing', () => {
    shallow(<EditUserRights messageService={messageService} />)
  })

  it('should display an appropriate message by default', () => {
    const wrap = shallow(<EditUserRights messageService={messageService} />)
    const header = wrap.find('#headerTitle').shallow().text()
    expect(header).toEqual('Edit User Rights')
  })

  it('should display the submit update user button on default', () => {
    const wrap = shallow(<EditUserRights messageService={messageService} />)
    const button = wrap.find('#buttonUdate')
    expect(button).toHaveLength(1)
  })

  it('should display the submit new user button on default', () => {
    const wrap = shallow(<EditUserRights messageService={messageService} />)
    const button = wrap.find('#submitNewUser')
    expect(button).toHaveLength(1)
  })
})
