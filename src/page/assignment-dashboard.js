import React, { Component } from 'react'
import { Table } from 'semantic-ui-react'
import AssignPerson from '../component/assign-person'
import './margins.css'

class AssignmentDashboard extends Component {
  constructor (props) {
    super(props)
    this.state = {
      httpService: this.props.httpService,
      authService: this.props.authService,
      messageService: this.props.messageService,
      path: '',
      redirect: false,
      data: [],
      userOptions: [],
      columns: [
        'Job Number',
        'Inspection Number',
        'Assign'
      ]
    }
  }

  renderTableRows () {
    let index = 0
    return this.state.data.map(array => {
      const { jobNumber, inspectionNumber, assigned } = array
      const assignedNames = []

      for (const id of assigned) {
        for (const user of this.state.userOptions) {
          if (Number(id) === Number(user.key)) {
            assignedNames.push(user.value)
          }
        }
      }

      return (
        <Table.Row key={index++} style={{ textAlign: 'center' }}>
          <Table.Cell>{jobNumber}</Table.Cell>
          <Table.Cell>{inspectionNumber}</Table.Cell>
          <Table.Cell>
            <AssignPerson
              httpService={this.state.httpService}
              authService={this.state.authService}
              messageService={this.state.messageService}
              jobNumber={jobNumber}
              inspectionNumber={inspectionNumber}
              users={this.state.userOptions}
              assigned={assignedNames}
            />
          </Table.Cell>
        </Table.Row>
      )
    })
  }

  renderTableHeaders () {
    return this.state.columns.map((colName, index) => {
      return <Table.HeaderCell key={index}> {colName}</Table.HeaderCell>
    })
  }

  componentDidMount () {
    if (typeof (this.state.httpService) !== 'undefined') {
      this.state.httpService.getWorkOrderDashboard()
        .then((response) => {
          if (typeof (response) !== 'undefined' && typeof (response) !== 'undefined') {
            const filtered = response.inspection_numbers.map((obj) => {
              return {
                jobNumber: obj.job_number,
                inspectionNumber: obj.inspection_number,
                assigned: obj.assigned
              }
            })

            const userChoices = response.users.map((user) => {
              const renamed = {}
              renamed.key = user.user_id
              renamed.value = user.user_username
              renamed.text = user.user_username
              return renamed
            })

            this.setState({
              data: filtered,
              userOptions: userChoices
            })
          }
        })
        .catch((err) => {
          this.state.messageService.showMessage(`Error: ${err}`)
        })
    }
  }

  render () {
    return (
      <div className='PageLayout'>
        <h1 id='title' style={{ textAlign: 'center' }}> Assignment Dashboard </h1>
        <Table striped celled color='red'>
          <Table.Header style={{ textAlign: 'center' }}>
            <Table.Row>
              {this.renderTableHeaders()}
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {this.renderTableRows()}
          </Table.Body>
        </Table>
      </div>
    )
  }
}

export default AssignmentDashboard
