import React from 'react'
import { shallow, configure } from 'enzyme'
import Import from './import'
import Adapter from 'enzyme-adapter-react-16'

configure({ adapter: new Adapter() })

describe('<Import />', () => {
  it('should render without crashing', () => {
    shallow(<Import />)
  })
})
