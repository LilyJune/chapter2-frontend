import React, { Component } from 'react'
import { Button, Header, Form, Segment } from 'semantic-ui-react'
import './margins.css'
const download = require('downloadjs')

class AdvancedInstructions extends Component {
  constructor (props) {
    super(props)
    this.state = {
      httpService: this.props.httpService,
      messageService: this.props.messageService,
      jobNumberInput: ''
    }
    this.handleGetFile = this.handleGetFile.bind(this)
    this.handleChange = this.handleChange.bind(this)
  }

  handleGetFile () {
    if (this.state.jobNumberInput !== '') {
      console.log(this.state.jobNumberInput)
      if (typeof (this.state.httpService) !== 'undefined') {
        this.state.httpService.getPdfInstructions(this.state.jobNumberInput)
          .then((res) => {
            console.log('response')
            console.log(res)
            return res.blob()
          }).then(blob => {
            console.log(blob)
            const url = `Advanced Instructions ${this.state.jobNumberInput} ${Date.now()}.zip`
            download(blob, url)
          }).catch((err) => {
            this.state.messageService.showMessage(`Data could not be retrieved: ${err}`)
          })
      }
    }
  }

  handleChange (e, object) {
    this.setState({ jobNumberInput: object.value })
  }

  render () {
    return (
      <div className='PageLayout' id='Import'>
        <Form>
          <Header as='h1'>
            Get Advanced Associated PDF Job Instructions
          </Header>
          <Form.Field>
            <Form.Input required label='Enter Job Number' onChange={this.handleChange} name='jobNumberInput' value={this.state.jobNumber} />
            <Segment>
              If pdf exists, you will then be presented with option of downloading the zip or opening its contents to see pdf.
            </Segment>
            <Button positive onClick={this.handleGetFile}>Get Instructions</Button>
          </Form.Field>
        </Form>
      </div>
    )
  }
}

export default AdvancedInstructions
