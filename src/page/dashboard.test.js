import React from 'react'
import { shallow, mount, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import Dashboard from './dashboard'
import MockService from '../../test-assets/mock-service'
import MessageService from '../service/message-service'

const messageService = new MessageService()
const mockService = new MockService()

configure({ adapter: new Adapter() })

describe('<Dashboard />', () => {
  it('should render a dashboard page without crashing', () => {
    shallow(<Dashboard messageService={messageService} />)
  })

  it('should display an empty table', () => {
    const wrap = shallow(<Dashboard messageService={messageService} />)
    expect(wrap.find('Table.Body')).toHaveLength(0)
  })

  it('should fetch data', async (done) => {
    const wrap = mount(<Dashboard httpService={mockService} messageService={messageService} />)
    setTimeout(() => {
      expect(wrap.state('originalData')).toHaveLength(2)
      done()
    }, 3000)
  })
})
