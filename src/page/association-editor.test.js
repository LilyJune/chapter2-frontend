import React from 'react'
import { shallow, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import Association from './association-editor'
import { Form } from 'semantic-ui-react'
import MessageService from '../service/message-service'

const messageService = new MessageService()

configure({ adapter: new Adapter() })

describe('<Association />', () => {
  it('should render without crashing', () => {
    shallow(<Association messageService={messageService} />)
  })

  it('should not display any cards by default', () => {
    const wrap = shallow(<Association messageService={messageService} />)
    expect(wrap.find('.card')).toHaveLength(0)
  })

  it('should display an appropriate message by default', () => {
    const wrap = shallow(<Association work_order={1} messageService={messageService} />)
    expect(wrap.find('Header').props().children).toEqual('Associate Job Number to Work Order Number: ')
  })

  it('should display two buttons', () => {
    const wrap = shallow(<Association work_order={1} messageService={messageService} />)
    const items = wrap.find(Form.Button)
    expect(items).toHaveLength(1)
  })

  it('should set open to true in state', () => {
    const wrap = shallow(<Association work_order={1} messageService={messageService} />)
    wrap.instance().handleOpen()
    expect(wrap.state().open).toEqual(true)
  })

  it('should set close to true in state', () => {
    const wrap = shallow(<Association work_order={1} messageService={messageService} />)
    wrap.instance().handleClose()
    expect(wrap.state().open).toEqual(false)
  })
})
