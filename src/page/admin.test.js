import React from 'react'
import { shallow, mount, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import Admin from './admin'
import MockService from '../../test-assets/mock-service'
import MessageService from '../service/message-service'

const messageService = new MessageService()
const mockService = new MockService()

configure({ adapter: new Adapter() })

describe('<Admin />', () => {
  it('admin page should render without crashing', () => {
    shallow(<Admin messageService={messageService} />)
  })

  it('should not display any cards by default', () => {
    const wrap = shallow(<Admin messageService={messageService} />)
    expect(wrap.find('.card')).toHaveLength(0)
  })

  it('should update data when data is fetched', async (done) => {
    const wrap = mount(<Admin service={mockService} jobNumber={1} messageService={messageService} />)
    setTimeout(() => {
      expect(wrap.state('data')).toHaveLength(2)
      done()
    }, 3000)
  })

  it('should render cards when data is updated', async (done) => {
    const wrap = mount(<Admin jobNumber={1} messageService={messageService} />)
    wrap.setState({
      data: [
        {
          inspection_number: 987654,
          frequency: '456789',
          setup_tech: 'techTest1',
          method: '345678',
          expected: 987654,
          name: '3456789',
          form_type: 'Input',
          job_number: 42069,
          tolerance: 45678
        }
      ]
    })
    setTimeout(() => {
      expect(wrap.find('.card')).toHaveLength(1)
      done()
    }, 3000)
  })
})
