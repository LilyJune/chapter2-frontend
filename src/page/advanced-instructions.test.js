import React from 'react'
import { shallow, configure } from 'enzyme'
import AdvancedInstructions from './advanced-instructions'
import Adapter from 'enzyme-adapter-react-16'

configure({ adapter: new Adapter() })

describe('<AdvancedInstructions />', () => {
  it('should render without crashing', () => {
    shallow(<AdvancedInstructions />)
  })
})
