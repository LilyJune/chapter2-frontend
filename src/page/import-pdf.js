import React, { Component } from 'react'
import { Progress, Header, Card, Form, Button } from 'semantic-ui-react'
import Dropzone from '../component/dropzone'
import './margins.css'

const API_SERVER_URL = process.env.REACT_APP_API_SERVER_URL

class ImportPDF extends Component {
  constructor (props) {
    super(props)
    this.state = {
      messageService: this.props.messageService,
      authService: this.props.authService,
      files: [],
      jobNumber: '',
      uploading: false,
      uploadProgress: {},
      successfullUploaded: false
    }

    this.handleFilesAdded = this.handleFilesAdded.bind(this)
    this.handleUploadFiles = this.handleUploadFiles.bind(this)
    this.sendRequest = this.sendRequest.bind(this)
    this.renderActions = this.renderActions.bind(this)
    this.handleChange = this.handleChange.bind(this)
  }

  handleChange (e, { name, value }) {
    this.setState({ [name]: value })
  }

  handleFilesAdded (files) {
    this.setState(prevState => ({
      files: prevState.files.concat(files)
    }))
  }

  async handleUploadFiles () {
    this.setState({ uploadProgress: {}, uploading: true })
    const promises = []
    this.state.files.forEach(file => {
      promises.push(this.sendRequest(file))
    })
    Promise.all(promises).then(() => {
      this.setState({ successfullUploaded: true, uploading: false })
    }).catch((e) => {
      this.state.messageService.showMessage(`Files failed to upload! This could be an issue with the server!\n${e}`)
      this.setState({ successfullUploaded: false, uploading: false })
    })
  }

  sendRequest (file) {
    return new Promise((resolve, reject) => {
      const req = new XMLHttpRequest()

      req.upload.addEventListener('progress', e => {
        if (e.lengthComputable) {
          const newPercent = { ...this.state.uploadProgress }
          newPercent[file.name] = {
            state: 'pending',
            percentage: (e.loaded / e.total) * 100
          }
          this.setState({ uploadProgress: newPercent })
        }
      })

      req.upload.addEventListener('load', e => {
        const newPercent = { ...this.state.uploadProgress }
        newPercent[file.name] = { state: 'done', percentage: 100 }
        this.setState({ uploadProgress: newPercent })
        resolve(req.response)
      })

      req.upload.addEventListener('error', e => {
        const newPercent = { ...this.state.uploadProgress }
        newPercent[file.name] = { state: 'error', percentage: 0 }
        this.setState({ uploadProgress: newPercent })
        reject(req.response)
      })

      const formData = new FormData()
      formData.append('file', file, file.name)
      formData.append('job_number', this.state.jobNumber)

      req.open('POST', `${API_SERVER_URL}import`)
      if (typeof (this.state.authService) !== 'undefined' && this.state.authService.loggedIn()) {
        req.setRequestHeader('Authorization', `Bearer ${this.state.authService.getToken()}`)
      }
      req.send(formData)
    })
  }

  renderProgress (file) {
    const uploadProgress = this.state.uploadProgress[file.name]
    return (
      <Progress
        active={uploadProgress ? uploadProgress.state === 'pending' : false}
        success={uploadProgress ? uploadProgress.state === 'done' : false}
        error={uploadProgress ? uploadProgress.state === 'error' : false}
        percent={uploadProgress ? uploadProgress.percentage : 0}
      />
    )
  }

  renderActions () {
    if (this.state.successfullUploaded) {
      return (
        <Button
          negative
          onClick={() => this.setState({ files: [], jobNumber: '', successfullUploaded: false, uploadProgress: {} })}
        >
          Clear
        </Button>
      )
    } else {
      return (
        <Form.Button
          positive
          disabled={this.state.files.length < 0 || this.state.uploading}
        >
          Upload
        </Form.Button>
      )
    }
  }

  render () {
    return (
      <div className='PageLayout' id='Import'>
        <Header as='h1'>
            Add PDF to Job Number
        </Header>
        <Form onSubmit={this.handleUploadFiles}>
          <div style={{ display: 'flex', justifyContent: 'center' }}>
            <Dropzone
              onFilesAdded={this.handleFilesAdded}
              disabled={this.state.uploading || this.state.successfullUploaded}
            />
          </div>
          <Form.Input
            style={{ marginTop: '10px', marginBottom: '10px' }}
            required
            onChange={this.handleChange}
            value={this.state.jobNumber}
            name='jobNumber'
            placeholder='Job Number'
            label='Job Number'
            disabled={this.state.files.length < 0 || this.state.uploading}
          />
          <div style={{ marginTop: '10px', marginBottom: '10px' }}>
            {this.state.files.length > 0 &&
              <Header as='h2'> Files to Upload </Header>}
            {this.state.files.map(file => {
              return (
                <Card key={file.name} fluid>
                  <div style={{ marginLeft: '10px', marginRight: '10px' }}>
                    <Card.Header as='h3'>
                      {file.name}
                    </Card.Header>
                    {this.renderProgress(file)}
                  </div>
                </Card>
              )
            })}
          </div>
          {this.state.files.length > 0 &&
            this.renderActions()}
        </Form>
      </div>
    )
  }
}

export default ImportPDF
