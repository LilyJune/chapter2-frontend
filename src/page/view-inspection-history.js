import React, { Component } from 'react'
import { Header, Grid, Segment, Card, List } from 'semantic-ui-react'
import './margins.css'

class ViewInspectionHistory extends Component {
  constructor (props) {
    super(props)
    this.state = {
      authService: this.props.authService,
      httpService: this.props.httpService,
      messageService: this.props.messageService,
      currentUser: '',
      inspectionHistory: []
    }
  }

  componentDidMount () {
    const currentComponent = this
    this.setState({
      currentUser: this.state.authService.getUsername()
    })
    if (typeof (this.state.httpService) !== 'undefined' && typeof (this.state.currentUser) !== 'undefined') {
      this.state.httpService.getInspectionHistory(this.state.authService.getID()).then((data) => {
        if (typeof (data) !== 'undefined') {
          data.json().then(function (temp) {
            currentComponent.setState({
              inspectionHistory: temp
            })
          })
        }
      }).catch((err) => {
        this.state.messageService.showMessage(`Data could not be retrieved: ${err}`)
      })
    }
  }

  render () {
    return (
      <div className='PageLayout'>
        <Header as='h2'>View Inspection history for: {this.state.currentUser}</Header>
        <Segment attached fluid='true'>
          <Header as='h3'>Inspection History</Header>
          <Grid centered columns='2'>
            <Grid.Row columns={1}>
              <Grid.Column>
                <Card.Group>
                  {
                    this.state.inspectionHistory.map(function (inspection, i) {
                      return (
                        <Card key={i}>
                          <Card.Content>
                            <Card.Header>Inspection Number: {inspection.inspection_number}</Card.Header>
                            <Card.Meta>Inspection Type: {inspection.inspection_type}</Card.Meta>
                            <Card.Description>
                             Date: {inspection.date}
                            </Card.Description>
                          </Card.Content>
                          <Card.Content>
                            <List>
                              <List.Item>machine: {inspection.machine}</List.Item>
                              <List.Item>characteristic: {inspection.characteristic}</List.Item>
                              <List.Item>job number: {inspection.job_number}</List.Item>
                              <List.Item>operator: {inspection.operator}</List.Item>
                              <List.Item>remark: {inspection.remark}</List.Item>
                              <List.Item>result: {inspection.result}</List.Item>
                              <List.Item>shift: {inspection.shift}</List.Item>
                              <List.Item>work order: {inspection.work_order}</List.Item>
                            </List>
                          </Card.Content>
                        </Card>
                      )
                    })
                  }
                </Card.Group>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Segment>
      </div>
    )
  }
}

export default ViewInspectionHistory
