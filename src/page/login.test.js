import React from 'react'
import { shallow, configure, mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import Login from './login'
import MessageService from '../service/message-service'

const messageService = new MessageService()

configure({ adapter: new Adapter() })

describe('<Login />', () => {
  it('should render without crashing', () => {
    shallow(<Login messageService={messageService} />)
  })

  it('should not crash if there is no service', () => {
    const wrap = mount(<Login messageService={messageService} />)
    wrap.instance().handleSubmit()
  })

  it('should contain blank fields by default', () => {
    const wrap = mount(<Login messageService={messageService} />)
    expect(wrap.find('input').at(0).props().value).toEqual('')
    expect(wrap.find('input').at(1).props().value).toEqual('')
  })

  it('should render fields based on value', async (done) => {
    const wrap = mount(<Login messageService={messageService} />)
    wrap.setState({
      username: 'testuser',
      password: 'testpass'
    })

    setTimeout(() => {
      expect(wrap.find('input').at(0).props().value).toEqual('testuser')
      expect(wrap.find('input').at(1).props().value).toEqual('testpass')
      done()
    }, 2000)
  })
})
