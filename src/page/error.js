import React, { Component } from 'react'
import { Icon, Header } from 'semantic-ui-react'
import './margins.css'

class Error extends Component {
  render () {
    return (
      <div className='PageLayout'>
        <Header>
          <Icon name='frown outline' />
          404 Error
        </Header>
        <p>The page you are trying to access does not exist</p>
      </div>
    )
  }
}
export default Error
