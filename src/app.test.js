import React from 'react'
import ReactDOM from 'react-dom'
import { shallow, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import App from './app'
import { Route } from 'react-router-dom'

configure({ adapter: new Adapter() })

describe('<App />', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div')
    ReactDOM.render(<App />, div)
    ReactDOM.unmountComponentAtNode(div)
  })

  it('renders without crashing using enzyme', () => {
    shallow(<App />)
  })

  it('renders routes correctly', () => {
    const wrapper = shallow(<App />)
    const pathMap = wrapper.find('Route').reduce((pathMap, route) => {
      const routeProps = route.props()
      pathMap[routeProps.path] = routeProps.component
      return pathMap
    }, {})
    expect(pathMap['/']).toHaveLength(1)
    expect(pathMap['/login']).toHaveLength(1)
    expect(pathMap['*']).toHaveLength(0)
  })

  it('should redirect to page if path exist', () => {
    const wrapper = shallow(<App />)
    const pathMap = wrapper.find(Route).reduce((pathMap, route) => {
      const routeProps = route.props()
      pathMap[routeProps.path] = routeProps.component
      return pathMap
    }, {})
    expect(pathMap['/']).toHaveLength(1)
  })
})
