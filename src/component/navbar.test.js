import React from 'react'
import { shallow, mount, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { Image } from 'semantic-ui-react'
import { MemoryRouter } from 'react-router-dom'
import NavBar from './navbar'

configure({ adapter: new Adapter() })

describe('<NavBar />', () => {
  it('should render without crashing', () => {
    shallow(<NavBar />)
  })

  it('should render with full DOM without crashing', () => {
    mount(<MemoryRouter> <NavBar /> </MemoryRouter>)
  })

  it('should select home as activeItem by default', () => {
    const wrap = shallow(<NavBar />)
    expect(wrap.state('activeItem')).toEqual('home')
  })

  it('should select home as activeItem upon clicking', () => {
    const wrap = mount(<MemoryRouter> <NavBar /> </MemoryRouter>)
    wrap.find('.item').at(1).simulate('click')
    expect(wrap.find('NavBar').state('activeItem')).toEqual('home')
  })

  it('should have a Menu', () => {
    const wrap = shallow(<NavBar />)
    expect(wrap.find('Menu')).toHaveLength(1)
  })

  it('should have three MenuItems by default', () => {
    const wrap = shallow(<NavBar />)
    expect(wrap.find('MenuItem')).toHaveLength(3)
  })

  it('should have image as first MenuItem', () => {
    const wrap = shallow(<NavBar />)
    expect(wrap.find('MenuItem').at(0).props().children).toMatchObject(<Image />)
  })
})
