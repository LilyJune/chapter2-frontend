import React from 'react'
import { shallow, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import ExportNav from './export-nav'

configure({ adapter: new Adapter() })

describe('<ExportNav />', () => {
  it('should render without crashing', () => {
    shallow(<ExportNav />)
  })

  it('should render a message box by default', () => {
    const wrap = shallow(<ExportNav />)
    const message = wrap.find('#messageBox').shallow().text()
    expect(message).toEqual('<MessageContent />')
  })

  it('should display the update route button on default', () => {
    const wrap = shallow(<ExportNav />)
    const button = wrap.find('#buttonUdate')
    expect(button).toHaveLength(1)
  })
})
