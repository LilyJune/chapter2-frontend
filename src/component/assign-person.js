import React, { Component } from 'react'
import _ from 'lodash'
import { Form } from 'semantic-ui-react'

class AssignPerson extends Component {
  constructor (props) {
    super(props)
    this.state = {
      jobNumber: this.props.jobNumber,
      inspectionNumber: this.props.inspectionNumber,
      httpService: this.props.httpService,
      authService: this.props.authService,
      messageService: this.props.messageService,
      users: this.props.users,
      originalAssigned: this.props.assigned,
      assigned: this.props.assigned,
      value: []
    }

    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleChange (e, { value }) {
    this.setState({
      assigned: value
    })
  }

  handleSubmit () {
    const assignedNames = _.difference(this.state.assigned, this.state.originalAssigned)
    const unassignedNames = _.difference(this.state.originalAssigned, this.state.assigned)
    const users = this.state.users
    const assigned = []
    const unassigned = []

    for (let i = 0; i < assignedNames.length; i++) {
      const name = assignedNames[i]
      for (let j = 0; j < users.length; j++) {
        if (users[j].value === name) {
          assigned.push(Number(users[j].key))
        }
      }
    }

    for (let i = 0; i < unassignedNames.length; i++) {
      const name = unassignedNames[i]
      for (let j = 0; j < users.length; j++) {
        if (users[j].value === name) {
          unassigned.push(users[j].key)
        }
      }
    }

    const dataToSubmit = {
      assign: assigned,
      unassign: unassigned
    }

    this.state.httpService.putAssigned(dataToSubmit, this.state.jobNumber, this.state.inspectionNumber)
      .then(() => {
        this.state.messageService.showMessage('Assignment Complete!')

        this.setState({
          originalAssigned: this.state.assigned
        })
      })
      .catch((err) => {
        this.state.messageService.showMessage(`${err}`)
      })
  }

  render () {
    return (
      <Form onSubmit={this.handleSubmit}>
        <Form.Dropdown
          placeholder='Assignee'
          multiple
          search
          selection
          options={this.state.users}
          value={this.state.assigned}
          onChange={this.handleChange}
        >
        </Form.Dropdown>
        <Form.Button negative>Assign</Form.Button>
      </Form>
    )
  }
}

export default AssignPerson
