import React from 'react'
import { shallow, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { MemoryRouter } from 'react-router-dom'
import LoginButton from './login-button'

configure({ adapter: new Adapter() })

describe('<LoginButton />', () => {
  it('should render without crashing', () => {
    shallow(<LoginButton />)
  })

  it('should have a link with role button', () => {
    const wrap = shallow(<MemoryRouter> <LoginButton /> </MemoryRouter>)
    expect(wrap.html()).toEqual(' <a class="ui primary button" role="button" href="/login">Login</a> ')
  })
})
