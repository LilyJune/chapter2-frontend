import React, { Component } from 'react'
import { Form, Header, Button, Segment } from 'semantic-ui-react'
import { Redirect } from 'react-router-dom'

class InspectionNavigation extends Component {
  constructor (props) {
    super(props)
    this.state = {
      authService: this.props.authService,
      jobNumber: '',
      workOrder: '',
      redirect: false
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleChange (e, { name, value }) {
    this.setState({ [name]: value })
  }

  handleSubmit () {
    if (this.state.jobNumber !== '' && this.state.workOrder !== '') {
      this.setState({ redirect: true })
    }
  }

  renderRedirect () {
    if (this.state.redirect && typeof (this.state.authService) !== 'undefined' && this.state.authService.loggedIn()) {
      const level = this.state.authService.getLevel()
      if (level === 1) {
        return <Redirect push to={`/operator/form/${this.state.jobNumber}/${this.state.workOrder}`} />
      } else if (level === 2 || level === 3) {
        return <Redirect push to={`/inspector/form/${this.state.jobNumber}/${this.state.workOrder}`} />
      }
    }
  }

  render () {
    return (
      <div id='inspectionNavigation'>
        {this.renderRedirect()}
        <Segment attached fluid>
          <Header as='h3'>Search For Inspection Form</Header>
          <Form>
            <Form.Input required id='jobNumberInput' onChange={this.handleChange} value={this.state.name} name='jobNumber' placeholder='Job Number' label='Job Number:' />
            <Form.Input required id='workOrderInput' onChange={this.handleChange} value={this.state.name} name='workOrder' placeholder='Work Order' label='Work Order:' />
            <Button positive onClick={this.handleSubmit}>Search for Form</Button>
          </Form>
        </Segment>
      </div>
    )
  }
}

export default InspectionNavigation
