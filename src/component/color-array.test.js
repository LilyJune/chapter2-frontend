import colorArray from './color-array'

describe('colorArray', () => {
  it('should be an array', () => {
    expect(Array.isArray(colorArray))
  })

  it('should have length 20', () => {
    expect(colorArray).toHaveLength(20)
  })
})
