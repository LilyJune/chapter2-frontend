import React, { Component } from 'react'
import { Form, Grid, Button, Message, Divider, Header } from 'semantic-ui-react'
import { Redirect } from 'react-router-dom'

const queryString = require('query-string')

class ExportNav extends Component {
  constructor (props) {
    super(props)
    this.state = {
      jobNumber: '',
      errorJobNumber: '',
      workOrder: '',
      redirect: false,
      path: ''
    }
    this.handleDirectPerError = this.handleDirectPerError.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.handleUpdateExport = this.handleUpdateExport.bind(this)
    this.renderRedirect = this.renderRedirect.bind(this)
  }

  handleChange (e, { name, value }) {
    this.setState({ [name]: value })
  }

  renderRedirect () {
    if (this.state.redirect) {
      return (
        <Redirect to={this.state.path} />
      )
    }
  }

  handleUpdateExport () {
    const params = {}
    const isJobNumber = this.state.jobNumber !== ''
    const isWorkOrder = this.state.workOrder !== ''
    if (isJobNumber) {
      params.job_number = this.state.jobNumber
    }
    if (isWorkOrder) {
      params.work_order = this.state.workOrder
    }
    if (isJobNumber || isWorkOrder) {
      this.setState({
        redirect: true,
        path: `/export?${queryString.stringify(params)}`
      })
    }
  }

  handleDirectPerError () {
    const params = {}
    const isERRJobNumber = this.state.errorJobNumber !== ''
    if (isERRJobNumber) {
      params.job_number = this.state.errorJobNumber
    }
    if (isERRJobNumber) {
      this.setState({
        redirect: true,
        path: `/export/error?${queryString.stringify(params)}`
      })
    }
  }

  render () {
    return (
      <div>
        <Form className='attached fluid segment'>
          <Form.Field>
            {this.renderRedirect()}
            <Message
              id='messageBox'
              attached
              header='Export Navigation'
              content='Enter jobNumber and workOrder below to filter the export page. The jobNumber is manadatory but the workOrder is an optional input'
            />
            <Grid centered columns='2'>
              <Grid.Row columns={1}>
                <Grid.Column>
                  <br /><Header as='h3'>Update Export</Header>
                  <Form.Group widths='equal'>
                    <Form.Input id='jobNumber' onChange={this.handleChange} value={this.state.jobNumber} name='jobNumber' placeholder='Job Number' label='Job Number:' />
                    <Form.Input id='workOrder' onChange={this.handleChange} value={this.state.workOrder} name='workOrder' placeholder='Work Order' label='Work Order:' />
                    <Button id='buttonUdate' positive onClick={this.handleUpdateExport}>Update Export</Button>
                  </Form.Group>
                  <Divider fullwidth />
                  <Header as='h3'>See Percent Error</Header>
                  <Form.Group widths='equal'>
                    <Form.Input id='errorJobNumber' onChange={this.handleChange} value={this.state.errorJobNumber} name='errorJobNumber' placeholder='Job Number' label='Job Number:' />
                    <Button positive onClick={this.handleDirectPerError}>See Percent Error</Button>
                  </Form.Group>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Form.Field>
        </Form>
      </div>
    )
  }
}

export default ExportNav
