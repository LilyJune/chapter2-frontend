import React, { Component } from 'react'
import { Button } from 'semantic-ui-react'
import { NavLink } from 'react-router-dom'

class LogoutButton extends Component {
  constructor (props) {
    super(props)
    this.state = {
      authService: this.props.authService
    }
  }

  render () {
    return (
      <Button
        primary
        as={NavLink} exact to='/'
        onClick={() => this.state.authService.logout()}
      >
        Logout
      </Button>
    )
  }
}

export default LogoutButton
