import React, { Component } from 'react'
import { VictoryChart, VictoryAxis, VictoryLine, VictoryTooltip, VictoryVoronoiContainer, VictoryGroup, VictoryScatter } from 'victory'
import colorArray from './color-array'

const groupBy = require('lodash.groupby')

class DataChart extends Component {
  constructor (props) {
    super(props)
    this.state = {
      data: this.props.data,
      displayData: []
    }

    this.structureData = this.structureData.bind(this)
  }

  structureData () {
    if (typeof (this.state.data) !== 'undefined' && this.state.data.length > 0 && this.state.displayData.length === 0) {
      let processedData = []
      processedData = this.state.data.map((entry, i) => {
        const date = new Date(entry.date)
        return (
          {
            id: `Job Number: ${entry.job_number} Inspection Number: ${entry.inspection_number}`,
            jobNumber: entry.job_number,
            inspectionNumber: entry.inspection_number,
            x: date,
            y: entry.result,
            remark: entry.remark,
            operator: entry.operator,
            workOrder: entry.work_order,
            machine: entry.machine,
            date: entry.date
          }
        )
      })

      let data = []
      data = groupBy(processedData, 'id')
      let finishedData = []

      for (const [key, value] of Object.entries(data)) {
        finishedData.push({
          id: key,
          data: value
        })
      }

      finishedData = finishedData.reverse()

      this.setState({
        displayData: finishedData
      })
    }
  }

  componentDidMount () {
    this.structureData()
  }

  render () {
    if (typeof (this.state.data) !== 'undefined' && this.state.displayData.length > 0) {
      return (
        <VictoryChart
          height={200}
          containerComponent={<VictoryVoronoiContainer />}
        >
          {this.state.displayData.map((dataGroup, i) => {
            return (
              <VictoryGroup
                key={i}
                color={colorArray[i % colorArray.length]}
                labels={({ datum }) => `Job Number: ${datum.jobNumber}\n Inspection Number: ${datum.inspectionNumber}\n Measurement: ${datum.y}\n  Date: ${datum.date}`}
                labelComponent={
                  <VictoryTooltip
                    style={{ fontSize: 6 }}
                  />
                }
                data={dataGroup.data}
              >
                <VictoryAxis
                  scale='time'
                  standalone={false}
                  tickFormat={date => date.toLocaleString('en-us', { month: 'short' })}
                  fixLabelOverlap
                />
                <VictoryLine />
                <VictoryScatter />
              </VictoryGroup>
            )
          })}
        </VictoryChart>
      )
    } else {
      return null
    }
  }
}

export default DataChart
