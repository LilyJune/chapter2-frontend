import React, { Component } from 'react'
import { Button } from 'semantic-ui-react'
import { NavLink } from 'react-router-dom'

class LoginButton extends Component {
  constructor (props) {
    super(props)
    this.state = {}
  }

  render () {
    return (
      <Button
        primary
        as={NavLink} exact to='/login'
      >
        Login
      </Button>
    )
  }
}

export default LoginButton
