import React from 'react'
import { shallow, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { MemoryRouter } from 'react-router-dom'
import LogoutButton from './logout-button'

configure({ adapter: new Adapter() })

describe('<LogoutButton />', () => {
  it('should render without crashing', () => {
    shallow(<LogoutButton />)
  })

  it('should have a link with role button', () => {
    const wrap = shallow(<MemoryRouter> <LogoutButton /> </MemoryRouter>)
    expect(wrap.html()).toEqual(' <a aria-current="page" class="ui primary button active" role="button" href="/">Logout</a> ')
  })
})
