import React, { Component } from 'react'
import { Button, Header, Icon, Modal } from 'semantic-ui-react'

class Messenger extends Component {
  constructor (props) {
    super(props)
    this.state = {
      messageService: this.props.messageService,
      open: false,
      message: ''
    }

    this.handleRender = this.handleRender.bind(this)
    this.handleClose = this.handleClose.bind(this)
  }

  componentDidMount () {
    if (typeof (this.state.messageService) !== 'undefined') {
      this.state.messageService.subscribe(this.handleRender)
    }
  }

  handleRender (message) {
    this.setState({ message: message, open: true })
  }

  handleClose () {
    this.setState({ open: false, message: '' })
  }

  render () {
    return (
      <Modal
        open={this.state.open}
        onClose={this.handleClose}
        size='small'
      >
        <Modal.Content>
          <Header as='h3'>
            {this.state.message}
          </Header>
        </Modal.Content>
        <Modal.Actions>
          <Button color='green' onClick={this.handleClose}>
            <Icon name='checkmark' /> OK
          </Button>
        </Modal.Actions>
      </Modal>
    )
  }
}

export default Messenger
