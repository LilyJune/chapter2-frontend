import React, { Component } from 'react'
import { Table } from 'semantic-ui-react'

class DataTable extends Component {
  constructor (props) {
    super(props)
    this.state = {
      data: this.props.data
    }

    this.findHeaders = this.findHeaders.bind(this)
    this.displayHeaders = this.displayHeaders.bind(this)
  }

  findHeaders () {
    if (typeof (this.state.data) !== 'undefined' && this.state.data.length > 0) {
      const headers = []
      for (const p in this.state.data[0]) {
        headers.push(`${p}`)
      }
      return headers
    }
    return null
  }

  displayHeaders (headerArray) {
    if (headerArray != null) {
      return (
        <Table.Row>
          {headerArray.map((header, i) => {
            return (<Table.HeaderCell key={i}>{header}</Table.HeaderCell>)
          })}
        </Table.Row>
      )
    }
  }

  render () {
    const headerArray = this.findHeaders()
    if (typeof (this.state.data) !== 'undefined' && this.state.data.length > 0) {
      return (
        <Table striped celled singleLine color='red'>
          <Table.Header>
            {this.displayHeaders(headerArray)}
          </Table.Header>
          <Table.Body>
            {this.state.data.map((map, i) => {
              return (
                <Table.Row key={i}>
                  {headerArray.map((header, i) => {
                    return (
                      <Table.Cell key={header}>
                        {`${map[header]}`}
                      </Table.Cell>
                    )
                  })}
                </Table.Row>
              )
            })}
          </Table.Body>
        </Table>
      )
    }
  }
}

export default DataTable
