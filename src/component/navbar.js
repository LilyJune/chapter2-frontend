import React, { Component } from 'react'
import { Menu, Dropdown, Image } from 'semantic-ui-react'
import { NavLink } from 'react-router-dom'
import LoginButton from './login-button'
import LogoutButton from './logout-button'
import Logo from '../assets/favicon-32x32.png'

class NavBar extends Component {
  constructor (props) {
    super(props)
    this.state = {
      authService: this.props.authService,
      activeItem: 'home'
    }
    this.handleItemClick = this.handleItemClick.bind(this)
  }

  handleItemClick (e, { name }) {
    this.setState({ activeItem: name })
  }

  render () {
    const { activeItem } = this.state
    const loggedIn = typeof (this.state.authService) !== 'undefined' && this.state.authService.loggedIn()

    return (
      <Menu inverted color='red'>
        <Menu.Item>
          <Image src={Logo} avatar />
        </Menu.Item>
        <Menu.Item>
          <Dropdown text='Pages'>
            <Dropdown.Menu>
              <Dropdown.Item
                text='Home'
                as={NavLink} exact to='/'
                active={activeItem === 'home'}
                onClick={this.handleItemClick}
              />
              {(typeof (this.state.authService) !== 'undefined' && this.state.authService.getLevel() >= 1) &&
                <Dropdown.Item
                  text='Export'
                  as={NavLink} exact to='/export'
                  active={activeItem === 'export'}
                  onClick={this.handleItemClick}
                />}
              {(typeof (this.state.authService) !== 'undefined' && this.state.authService.getLevel() >= 1) &&
                <Dropdown.Item
                  text='Inspection History'
                  as={NavLink} exact to='/user/viewInspectionHistory'
                  active={activeItem === 'inspection history'}
                  onClick={this.handleItemClick}
                />}
              {(typeof (this.state.authService) !== 'undefined' && this.state.authService.getLevel() >= 1) &&
                <Dropdown.Item
                  text='View Advanced Instructions'
                  as={NavLink} exact to='/user/viewAdvancedInstructions'
                  active={activeItem === 'View Advanced Instructions'}
                  onClick={this.handleItemClick}
                />}
              {(typeof (this.state.authService) !== 'undefined' && this.state.authService.getLevel() >= 3) &&
                <Dropdown.Item
                  text='Import'
                  as={NavLink} exact to='/import'
                  active={activeItem === 'import'}
                />}
              {(typeof (this.state.authService) !== 'undefined' && this.state.authService.getLevel() >= 3) &&
                <Dropdown.Item
                  text='Add PDF'
                  as={NavLink} exact to='/import/pdf'
                  active={activeItem === 'add PDF'}
                />}
              {(typeof (this.state.authService) !== 'undefined' && this.state.authService.getLevel() >= 3) &&
                <Dropdown.Item
                  text='Edit Association '
                  as={NavLink} exact to='/association_editor'
                  active={activeItem === 'edit association'}
                />}
              {(typeof (this.state.authService) !== 'undefined' && this.state.authService.getLevel() >= 3) &&
                <Dropdown.Item
                  text='Edit User'
                  as={NavLink} exact to='/admin/edit_user'
                  active={activeItem === 'edit user'}
                  onClick={this.handleItemClick}
                />}
              {(typeof (this.state.authService) !== 'undefined' && this.state.authService.getLevel() >= 3) &&
                <Dropdown.Item
                  text='Add Job Number'
                  as={NavLink} exact to='/add_job_number'
                  active={activeItem === 'add job number'}
                  onClick={this.handleItemClick}
                />}
            </Dropdown.Menu>
          </Dropdown>
        </Menu.Item>
        <Menu.Menu position='right'>
          {loggedIn &&
            <Menu.Item>
              {this.state.authService.getUsername()}
            </Menu.Item>}
          <Menu.Item>
            {!loggedIn && <LoginButton authService={this.state.authService} />}
            {loggedIn && <LogoutButton authService={this.state.authService} />}
          </Menu.Item>
        </Menu.Menu>
      </Menu>
    )
  }
}

export default NavBar
