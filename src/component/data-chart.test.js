import React from 'react'
import { shallow, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import DataChart from './data-chart'

configure({ adapter: new Adapter() })

const data = [
  { header00: 'test00', header01: 'test01' },
  { header10: 'test10', header01: 'test11' },
  { header20: 'test20', header21: 'test21' }
]

describe('<DataChart />', () => {
  it('should render without data without crashing', () => {
    shallow(<DataChart />)
  })

  it('should render with data without crashing', () => {
    shallow(<DataChart data={data} />)
  })
})
