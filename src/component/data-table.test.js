import React from 'react'
import { shallow, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import DataTable from './data-table'

configure({ adapter: new Adapter() })

const data = [
  { header00: 'test00', header01: 'test01' },
  { header10: 'test10', header01: 'test11' },
  { header20: 'test20', header21: 'test21' }
]

describe('<DataTable />', () => {
  it('should render without data without crashing', () => {
    shallow(<DataTable />)
  })

  it('should render with data without crashing', () => {
    shallow(<DataTable data={data} />)
  })

  it('should render a table while given data', () => {
    const wrap = shallow(<DataTable data={data} />)
    expect(wrap.find('Table')).toHaveLength(1)
  })

  it('should render the correct number of rows in the table while given data', () => {
    const wrap = shallow(<DataTable data={data} />)
    expect(wrap.find('TableRow')).toHaveLength(4)
  })

  it('should render the correct number of HeaderCells in the table while given data', () => {
    const wrap = shallow(<DataTable data={data} />)
    expect(wrap.find('TableHeaderCell')).toHaveLength(2)
  })

  it('should render the correct number of Headers in the table while given data', () => {
    const wrap = shallow(<DataTable data={data} />)
    expect(wrap.find('TableHeader')).toHaveLength(1)
  })

  it('should render the correct number of TableCells in the table while given data', () => {
    const wrap = shallow(<DataTable data={data} />)
    expect(wrap.find('TableCell')).toHaveLength(6)
  })

  it('should not return headers if the arg is null', () => {
    const dataTable = new DataTable({ data: { data } })
    expect(dataTable.displayHeaders()).toEqual(undefined)
  })
})
