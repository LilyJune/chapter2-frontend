import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import Error from '../../page/error'

function PrivateRoute ({ level, authService, component: Component, authed, ...rest }) {
  return (
    <Route
      {...rest} render={(props) => {
        if (typeof (authService) !== 'undefined') {
          if (authService.loggedIn() && authService.getLevel() >= level) {
            return <Component {...props} />
          } else if (authService.loggedIn()) {
            return <Error />
          } else {
            return <Redirect to='/login' />
          }
        }
      }}
    />
  )
}

export default PrivateRoute
