import React, { Component } from 'react'
import './dropzone.css'

class Dropzone extends Component {
  constructor (props) {
    super(props)
    this.state = { hightlight: false }
    this.fileInputRef = React.createRef()

    this.handleOpenFileDialog = this.handleOpenFileDialog.bind(this)
    this.handleFilesAdded = this.handleFilesAdded.bind(this)
    this.handleDragOver = this.handleDragOver.bind(this)
    this.handleDragLeave = this.handleDragLeave.bind(this)
    this.handleDrop = this.handleDrop.bind(this)
  }

  handleOpenFileDialog () {
    if (!this.props.disabled) {
      this.fileInputRef.current.click()
    }
  }

  handleFilesAdded (e) {
    if (!this.props.disabled) {
      const files = e.target.files
      if (this.props.onFilesAdded) {
        const array = this.fileListToArray(files)
        this.props.onFilesAdded(array)
      }
    }
  }

  handleDragOver (e) {
    e.preventDefault()

    if (!this.props.disabled) {
      this.setState({ hightlight: true })
    }
  }

  handleDragLeave () {
    this.setState({ hightlight: false })
  }

  handleDrop (e) {
    e.preventDefault()

    if (!this.props.disabled) {
      const files = e.dataTransfer.files
      if (this.props.onFilesAdded) {
        const array = this.fileListToArray(files)
        this.props.onFilesAdded(array)
      }
      this.setState({ hightlight: false })
    }
  }

  fileListToArray (list) {
    const array = []
    for (let i = 0; i < list.length; i++) {
      array.push(list.item(i))
    }
    return array
  }

  render () {
    return (
      <div
        className={`Dropzone ${this.state.hightlight ? 'Highlight' : ''}`}
        onDragOver={this.handleDragOver}
        onDragLeave={this.handleDragLeave}
        onDrop={this.handleDrop}
        onClick={this.handleOpenFileDialog}
        style={{ cursor: this.props.disabled ? 'default' : 'pointer' }}
      >
        <input
          ref={this.fileInputRef}
          className='FileInput'
          type='file'
          multiple
          onChange={this.handleFilesAdded}
        />
        <span>Upload Files</span>
      </div>
    )
  }
}

export default Dropzone
