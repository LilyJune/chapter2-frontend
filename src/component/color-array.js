const colorArray = [
  '#32C12C',
  '#009888',
  '#3E49BB',
  '#526EFF',
  '#7F4FC9',
  '#87C735',
  '#CDE000',
  '#00A5F9',
  '#00BCD9',
  '#682CBF',
  '#FFEF00',
  '#FF9A00',
  '#FF9A00',
  '#7C5547',
  '#5F7D8E',
  '#FFCD00',
  '#FF5500',
  '#D40C00',
  '#50342C',
  '#9E9E9E'
]

export default colorArray
