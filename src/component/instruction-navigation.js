import React, { Component } from 'react'
import { Form, Header, Button, Segment } from 'semantic-ui-react'
import { Redirect } from 'react-router-dom'

class InstructionNavigation extends Component {
  constructor (props) {
    super(props)
    this.state = {
      authService: this.props.authService,
      jobNumber: '',
      redirect: false
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleChange (e, { name, value }) {
    this.setState({ [name]: value })
  }

  handleSubmit () {
    if (this.state.jobNumber !== '' && this.state.workOrder !== '') {
      this.setState({ redirect: true })
    }
  }

  renderRedirect () {
    if (this.state.redirect && typeof (this.state.authService) !== 'undefined' && this.state.authService.loggedIn() && this.state.authService.getLevel() === 3) {
      return <Redirect push to={`/admin/instructions/${this.state.jobNumber}`} />
    }
  }

  render () {
    return (
      <div id='instructionNavigation'>
        {this.renderRedirect()}
        <Segment attached fluid='true'>
          <Header as='h3'>Add Instructions to Job Number</Header>
          <Form>
            <Form.Input required id='jobNumberInput' onChange={this.handleChange} value={this.state.name} name='jobNumber' placeholder='Job Number' label='Job Number:' />
            <Button positive onClick={this.handleSubmit}>Navigate </Button>
          </Form>
        </Segment>
      </div>
    )
  }
}

export default InstructionNavigation
