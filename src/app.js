import React from 'react'
import { Switch, BrowserRouter, Route } from 'react-router-dom'
import NavBar from './component/navbar'
import Messenger from './component/messenger'
import PrivateRoute from './component/routing/private-route'
import Home from './page/home'
import Export from './page/export'
import Admin from './page/admin'
import Inspection from './page/inspection'
import Error from './page/error'
import Login from './page/login'
import AdminJobAddingForm from './page/add-job-number'
import WorkOrderJobNumberAssociation from './page/association-editor.js'
import Import from './page/import'
import ImportPDF from './page/import-pdf'
import EditUser from './page/edit-user-rights'
import ViewInspectionHistory from './page/view-inspection-history'
import PercentError from './page/percent-error'
import AdvancedInstructions from './page/advanced-instructions'

const App = (props) => {
  return (
    <div className='App'>
      <BrowserRouter>
        <Messenger messageService={props.messageService} />
        <NavBar authService={props.authService} />
        <Switch>
          <Route
            path='/' exact component={({ match }) =>
              <Home {...props} />}
          />
          <Route
            path='/login' exact component={({ match }) =>
              <Login authService={props.authService} messageService={props.messageService} />}
          />
          <PrivateRoute
            level={1}
            authService={props.authService}
            path='/export/error'
            component={() =>
              <PercentError
                httpService={props.httpService} messageService={props.messageService}
              />}
          />
          <PrivateRoute
            level={1}
            authService={props.authService}
            path='/export'
            component={() =>
              <Export
                httpService={props.httpService} messageService={props.messageService}
              />}
          />
          <PrivateRoute
            level={1}
            authService={props.authService}
            path='/user/viewInspectionHistory'
            component={({ match }) =>
              <ViewInspectionHistory
                authService={props.authService} httpService={props.httpService} messageService={props.messageService}
              />}
          />
          <PrivateRoute
            level={1}
            authService={props.authService}
            path='/user/viewAdvancedInstructions'
            component={({ match }) =>
              <AdvancedInstructions
                authService={props.authService} httpService={props.httpService} messageService={props.messageService}
              />}
          />
          <PrivateRoute
            level={3}
            authService={props.authService}
            path='/admin/instructions/:jobNumber' component={({ match }) =>
              <Admin service={props.httpService} jobNumber={match.params.jobNumber} messageService={props.messageService} />}
          />
          <PrivateRoute
            level={3}
            authService={props.authService}
            path='/add_job_number' component={({ match }) =>
              <AdminJobAddingForm service={props.httpService} messageService={props.messageService} />}
          />
          <PrivateRoute
            level={3}
            authService={props.authService}
            path='/import/pdf' component={({ match }) => <ImportPDF messageService={props.messageService} authService={props.authService} />}
          />
          <PrivateRoute
            level={3}
            authService={props.authService}
            path='/import' component={({ match }) => <Import messageService={props.messageService} authService={props.authService} />}
          />
          <PrivateRoute
            level={3}
            authService={props.authService}
            path='/admin/edit_user' component={({ match }) =>
              <EditUser service={props.httpService} authService={props.authService} messageService={props.messageService} />}
          />
          <PrivateRoute
            level={1}
            authService={props.authService}
            path='/operator/form/:jobNumber/:workOrder'
            component={({ match }) =>
              <Inspection
                authService={props.authService}
                messageService={props.messageService}
                service={props.httpService}
                jobNumber={match.params.jobNumber}
                workOrder={match.params.workOrder}
                inspectionType='Operator'
              />}
          />
          <PrivateRoute
            level={3}
            authService={props.authService}
            path='/association_editor'
            component={({ match }) =>
              <WorkOrderJobNumberAssociation service={props.httpService} work_order={match.params.work_order} messageService={props.messageService} />}
          />
          <Route
            level={2}
            authService={props.authService}
            path='/inspector/form/:jobNumber/:workOrder' component={({ match }) =>
              <Inspection
                authService={props.authService}
                service={props.httpService}
                messageService={props.messageService}
                jobNumber={match.params.jobNumber}
                workOrder={match.params.workOrder}
                inspectionType='Inspector'
              />}
          />
          <Route path='*' component={Error} />
        </Switch>
      </BrowserRouter>
    </div>
  )
}

export default App
