import decode from 'jwt-decode'

const API_SERVER_URL = process.env.REACT_APP_API_SERVER_URL

class AuthService {
  subscribe (onChange) {
    this.onChange = onChange
  }

  _handleChange () {
    if (typeof (this.onChange) !== 'undefined' && this.onChange !== null) this.onChange()
  }

  login (username, password) {
    return this._makeRequest('login', {
      method: 'POST',
      body: {
        username: username,
        password: password
      }
    }).then(res => {
      this.setToken(res.token)
      return Promise.resolve(res)
    })
  }

  loggedIn () {
    const token = this.getToken()
    return token !== '' && token !== null && !this.isTokenExpired(token)
  }

  isTokenExpired (token) {
    try {
      const decoded = decode(token)
      return (decoded.exp < Date.now() / 1000)
    } catch (err) {
      console.warn(`Error: ${err}`)
      return false
    }
  }

  setToken (token) {
    localStorage.setItem('id_token', token)
    this._handleChange()
  }

  getToken () {
    return localStorage.getItem('id_token')
  }

  getName () {
    if (this.loggedIn()) {
      try {
        return decode(localStorage.getItem('id_token')).name
      } catch (err) {
        console.warn(`Error: ${err}`)
      }
    }
    return ''
  }

  getUsername () {
    if (this.loggedIn()) {
      try {
        return decode(localStorage.getItem('id_token')).username
      } catch (err) {
        console.warn(`Error: ${err}`)
      }
    }
    return ''
  }

  getID () {
    if (this.loggedIn()) {
      try {
        return decode(localStorage.getItem('id_token')).id
      } catch (err) {
        console.warn(`Error: ${err}`)
      }
    }
    return ''
  }

  getLevel () {
    if (this.loggedIn()) {
      try {
        return decode(localStorage.getItem('id_token')).level
      } catch (err) {
        console.warn(`Error: ${err}`)
      }
    }
    return 0
  }

  logout () {
    localStorage.removeItem('id_token')
    this._handleChange()
  }

  _makeRequest (url, options) {
    options.headers = {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    }

    if (this.loggedIn()) {
      options.headers.Authorization = `Bearer ${this.getToken()}`
    }

    if (options.body) {
      options.body = JSON.stringify(options.body)
    }

    return fetch(API_SERVER_URL + url, options).then((response) => {
      if (response.ok) {
        return response.json()
      } else {
        return response.json().then(({ errors }) => {
          const error = new Error(response.statusText)
          response.errorMessages = errors
          throw error
        })
      }
    })
  }
}

export default AuthService
