import AuthService from './auth-service'

describe('AuthService', () => {
  it('should set and get the token correctly', () => {
    const auth = new AuthService()
    auth.setToken('testToken')
    expect(auth.getToken()).toEqual('testToken')
  })

  it('should return a blank username by default', () => {
    const auth = new AuthService()
    expect(auth.getUsername()).toEqual('')
  })

  it('should return a 0 level by default', () => {
    const auth = new AuthService()
    expect(auth.getLevel()).toEqual(0)
  })

  it('should return a blank id by default', () => {
    const auth = new AuthService()
    expect(auth.getID()).toEqual('')
  })

  it('should logout correctly', () => {
    const auth = new AuthService()
    auth.setToken('testToken')
    auth.logout()
    expect(auth.getToken()).toEqual(null)
  })
})
