import AuthService from './auth-service'

const API_SERVER_URL = process.env.REACT_APP_API_SERVER_URL

const authService = new AuthService()

class HTTPService {
  putAssigned (data, jobNumber, inspectionNumber) {
    const slug = `assign?job_number=${jobNumber}&inspection_number=${inspectionNumber}`
    return this._makeRequest(slug, {
      method: 'PUT',
      body: data
    }).then((res) => {
      return res
    })
  }

  getVisualData (slug) {
    const url = `data${slug}`

    return this._makeRequest(url, {
      method: 'GET'
    }).then((res) => {
      return res
    })
  }

  getInspectionHistory (userId) {
    let url = 'inspection_history'
    if (typeof (userId) !== 'undefined') {
      url = `${url}/${userId}`
    }
    return this._makeFileRequest(url, {
      method: 'GET'
    }).then((res) => {
      return res
    })
  }

  getExportedFiles (slug) {
    const url = `export${slug}`
    return this._makeFileRequest(url, {
      method: 'GET'
    }).then((res) => {
      return res
    })
  }

  getWorkOrderDashboard () {
    const slug = 'assign'
    return this._makeRequest(slug, { method: 'GET' })
      .then((res) => {
        return res
      })
  }

  getUserAssignments () {
    const slug = 'myAssignments'
    return this._makeRequest(slug, { method: 'GET' })
      .then((res) => {
        return res
      })
  }

  getWorkOrderJobNumberRelationship (workOrder) {
    const slug = `association/${workOrder}`
    return this._makeRequest(slug, { method: 'GET' })
      .then((res) => {
        return res
      })
  }

  getPdfInstructions (slug) {
    const url = `pdf?job_number=${slug}`
    return this._makeFileRequest(url, {
      method: 'GET'
    }).then((res) => {
      return res
    })
  }

  putWorkOrderJobNumberRelationship (workOrder, data) {
    const url = `associations/${workOrder}`
    return this._makeRequest(url, {
      method: 'PUT',
      body: data
    }).then((res) => (
      res
    ))
  }

  putInstructions (jobNumber, data) {
    const url = `instructions/${jobNumber}`
    return this._makeRequest(url, {
      method: 'PUT',
      body: data
    }).then((res) => (
      res
    ))
  }

  putUpdateCreateAccount (data) {
    const slug = 'user'
    return this._makeRequest(slug,
      {
        method: 'PUT',
        body: data
      }
    ).then((res) => (
      res
    ))
  }

  postInspection (jobNumber, workOrder, data) {
    const slug = `form/${jobNumber}/${workOrder}`
    return this._makeRequest(slug,
      {
        method: 'POST',
        body: data
      }
    ).then((res) => (
      res
    ))
  }

  getInstructions (jobNumber) {
    const slug = `instructions/${jobNumber}`
    return this._makeRequest(slug, { method: 'GET' })
      .then((res) => {
        return res
      })
  }

  getCustomForm (jobNumber, workOrder) {
    const slug = `form/${jobNumber}/${workOrder}`
    return this._makeRequest(slug, { method: 'GET' })
      .then((res) => {
        return res
      })
  }

  postAddJobNumber (data) {
    const slug = 'addJobNumber'
    return this._makeRequest(slug,
      {
        method: 'POST',
        body: data
      }
    ).then((res) => (
      res
    ))
  }

  getInstructionsByUserType (slug) {
    const url = `instructions/worker/${slug}`
    return this._makeRequest(url, {
      method: 'GET'
    }).then((res) => {
      return res
    })
  }

  getInstructionCountByUserType () {
    const slug = 'instructions/count'
    return this._makeRequest(slug, { method: 'GET' })
      .then((res) => {
        return res
      })
  }

  _makeRequest (url, options) {
    options.headers = {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    }

    if (authService.loggedIn()) {
      options.headers.Authorization = `Bearer ${authService.getToken()}`
    }

    if (options.body) {
      options.body = JSON.stringify(options.body)
    }

    return fetch(API_SERVER_URL + url, options).then((response) => {
      if (response.ok) {
        return response.json()
      } else {
        return response.json().then(({ errors }) => {
          const error = new Error(response.statusText)
          response.errorMessages = errors
          throw error
        })
      }
    })
  }

  _makeFileRequest (url, options) {
    options.headers = {
      Accept: 'text/csv',
      'Content-Type': 'text/csv'
    }
    if (authService.loggedIn()) {
      options.headers.Authorization = `Bearer ${authService.getToken()}`
    }

    if (options.body) {
      options.body = JSON.stringify(options.body)
    }

    return fetch(API_SERVER_URL + url, options).then((response) => {
      if (response.ok) {
        return response
      } else {
        return response.json().then(({ errors }) => {
          const error = new Error(response.statusText)
          response.errorMessages = errors
          throw error
        })
      }
    })
  }
}

export default HTTPService
