class MessageService {
  subscribe (onChange) {
    this.onChange = onChange
  }

  _handleChange (message) {
    if (typeof (this.onChange) !== 'undefined' && this.onChange !== null) this.onChange(message)
  }

  showMessage (message) {
    this._handleChange(message)
  }
}

export default MessageService
