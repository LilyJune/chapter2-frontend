import './config.js'
import React from 'react'
import ReactDOM from 'react-dom'
import App from './app'
import * as serviceWorker from './serviceWorker'
import 'semantic-ui-css/semantic.min.css'
import AuthService from './service/auth-service'
import HTTPService from './service/http-service'
import MessageService from './service/message-service'

const authService = new AuthService()
const httpService = new HTTPService()
const messageService = new MessageService()

const renderApp = () => {
  ReactDOM.render(<App authService={authService} httpService={httpService} messageService={messageService} />, document.getElementById('root'))
}

authService.subscribe(renderApp)
renderApp()

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
