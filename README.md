# Frontend

## Frontend Development Environment Guide

### Tools

* Git
* NodeJS
* NPM
* ReactJS
* jest
* Gitlab-CI

### ENV file

To run this your will need to create a `.env` file at the root directory of this project. It should be in this format:

```
REACT_APP_API_SERVER_URL=''
```

You should put the url or ip address to the backend there. For example:

```
REACT_APP_API_SERVER_URL='http://localhost:8080/'
```

### Installing Tools

#### Windows

##### Git

Git is the tool used for version control for this project. Git can be installed on windows by simply downloading and running the executable from [here](https://git-scm.com/download/win).

After installation it can be checked in the command line with:

```
git -v
```

##### NodeJS & NPM

NodeJS and NPM come with each other. On Windows this is a simple install. Simply download and run the stable version installer of NodeJS from [here](https://nodejs.org/en/). The stable one always has an even number before the first dot.

After they are installed you should be able to confirm the installation by entering a command line and entering:

```
npm -v
```

and

```
node -v
```


##### Everything else

Open the frontend folder. In command line, this can be done by being in the folder above and entering `cd frontend`.

To install everything else you will need to run the command `npm install`. Congratz! you have the entire dev environment installed!

You can now start the project with the script `npm start`!


#### Ubuntu

[Download the latest version](https://ubuntu.com/download/desktop)


##### Git

On ubuntu you can do this all through a terminal:

```
sudo apt update
sudo apt install git
```

After installation it can be checked in the command line with:

```
git -v
```


##### NodeJS & NPM

On ubuntu extensive instructions can be found [here](https://github.com/nodesource/distributions/blob/master/README.md#snap).

For a simplified instructions, we are going to install the snap package manager in order to access this package.

```
sudo apt update
sudo apt install snapd
```

This may take a bit.

After this is done you will be able to install node and npm:

```
sudo snap install node --classic --channel=12
```

After they are installed you should be able to confirm the installation by entering a command line and entering:

```
npm -v
```

and

```
node -v
```


##### Everything else

Open the frontend folder. In a terminal, this can be done by being in the folder above and entering `cd ./frontend/`.

To install everything else you will need to run the command `npm install`. Congratz! you have the entire dev environment installed!

You can now start the project with the script `npm start`!



## Available Scripts

In the project directory, you can run:


### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.


### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.


### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.


### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.


### `npm run standardize`

This will standardize the code through standardjs.

## Frontend Production Environment Guide

### Tools

* Git
* NodeJS
* NPM
* ReactJS
* jest
* Gitlab-CI

* Note: any of the commands for the development and production guide are the same


### In Tools

#### Windows

##### Git

Git is the tool used for version control for this project. Git can be installed on windows by simply downloading and running the executable from [here](https://git-scm.com/download/win).

After installation it can be checked in the command line with:

```
git -v
```

##### NodeJS & NPM

NodeJS and NPM come with each other. On Windows this is a simple install. Simply download and run the stable version installer of NodeJS from [here](https://nodejs.org/en/). The stable one always has an even number before the first dot.

After they are installed you should be able to confirm the installation by entering a command line and entering:

```
npm -v
```

and

```
node -v
```


##### Everything else

Open the frontend folder. In command line, this can be done by being in the folder above and entering `cd frontend`.

To install everything else you will need to run the command `npm install`. Congratz! you have the entire dev environment installed!

You can now start the project with the script `npm start`!


#### Ubuntu

[Download the latest version](https://ubuntu.com/download/desktop)


##### Git

On ubuntu you can do this all through a terminal:

```
sudo apt update
sudo apt install git
```

After installation it can be checked in the command line with:

```
git -v
```


##### NodeJS & NPM

On ubuntu extensive instructions can be found [here](https://github.com/nodesource/distributions/blob/master/README.md#snap).

For a simplified instructions, we are going to install the snap package manager in order to access this package.

```
sudo apt update
sudo apt install snapd
```

This may take a bit.

After this is done you will be able to install node and npm:

```
sudo snap install node --classic --channel=12
```

After they are installed you should be able to confirm the installation by entering a command line and entering:

```
npm -v
```

and

```
node -v
```


##### Everything else

Open the frontend folder. In a terminal, this can be done by being in the folder above and entering `cd ./frontend/`.

To install everything else you will need to run the command `npm install`. Congratz! you have the entire dev environment installed!

You can now start the project with the script `npm start`!



## Available Scripts

In the project directory, you can run:


### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.


### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.


### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.


### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.


### `npm run standardize`

This will standardize the code through standardjs.
