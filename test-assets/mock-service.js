class MockService {
  async getCustomForm (jobNumber, workOrder) {
    return (this.getData(jobNumber, workOrder))
  }

  async getData (jobNumber, workOrder) {
    return ({
      job_number: { jobNumber },
      work_order: { workOrder },
      characteristics: [
        {
          name: 'test1name',
          method: 'test1method',
          expected: '1',
          tolerance: '0.01',
          form_type: 'Input',
          frequency: 'test1frequency'
        },
        {
          name: 'test2name',
          method: 'test2method',
          expected: '2',
          tolerance: '0.02',
          form_type: 'Checkbox',
          frequency: 'test2frequency'
        },
        {
          name: 'test3name',
          method: 'test3method',
          expected: '3',
          tolerance: '0.03',
          form_type: 'Input',
          frequency: 'test3frequency'
        }]
    })
  }

  async getInstructions (jobNumber) {
    return (this.getGoodInstructions())
  }

  async getGoodInstructions () {
    return ({
      characteristics: [
        {
          inspection_number: 987654,
          frequency: '456789',
          setup_tech: 'techTest1',
          method: '345678',
          expected: 987654,
          name: '3456789',
          form_type: 'Input',
          job_number: 42069,
          tolerance: 45678
        },
        {
          inspection_number: 12345678,
          frequency: '2345678',
          setup_tech: 'techTest2',
          method: '456',
          expected: 5467890,
          name: 'qwertyuio',
          form_type: 'Input',
          job_number: 42069,
          tolerance: 9876543
        }
      ]
    })
  }

  async getInstructionCountByUserType () {
    return ({
      count: '300'
    })
  }

  async getUserAssignments () {
    return (
      [{}]
    )
  }

  async getInstructionsByUserType () {
    return ({
      type: '3',
      data: [{
        job_number: '1123',
        inspection_number: 12,
        frequency: 'once a day',
        inspection_type: 'operator',
        form_type: 'visual',
        setup_tech: 'max',
        method: 'visual method',
        expected: 'yes or no expected',
        tolerance: 'there is no tolerance'
      },
      {
        job_number: '523',
        inspection_number: 2,
        frequency: 'once a day',
        inspection_type: 'inspector',
        form_type: 'visual',
        setup_tech: 'max',
        method: 'visual method',
        expected: 'yes or no expected',
        tolerance: 'there is no tolerance'
      }]
    })
  }

  async getVisualData (url) {
    if (url.includes('?')) {
      const data = [
        { header00: 'test00', header01: 'test01' },
        { header10: 'test10', header01: 'test11' },
        { header20: 'test20', header21: 'test21' }
      ]
      if (url.includes('&')) {
        return new Promise((resolve, reject) => {
          reject(new Error())
        })
      }
      return new Promise((resolve, reject) => {
        resolve(data)
      })
    } else {
      return new Promise((resolve, reject) => {
        resolve()
      })
    }
  }
}

export default MockService
